package interfaces;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.border.SoftBevelBorder;

import componentes.BotonOk;
import componentes.MiCampoContrasena;
import componentes.MiCampoTexto;
import componentes.MiLabel;
import excepciones.MisExcepciones;
import fitpointsDAO.UsuarioDAO;

import javax.swing.border.BevelBorder;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

final public class Logear extends JPanel {
	private FitpointJF ventanaMadre;
	private MiCampoContrasena campoContrasena;
	private MiLabel lblPassword;
	private MiCampoTexto campoEmail; 

	/**
	 * Create the panel.
	 */
	public Logear(FitpointJF fjf) {
		super();
	        this.ventanaMadre=fjf;
		    setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, new Color(255, 255, 0), new Color(255, 255, 0)));	
		    setSize(new Dimension(900, 700));
		    setBackground(Color.WHITE);
			setLayout(null);
			
			JLabel lblNewLabel = new JLabel("");
		     String path="./src/img/fitpoints.png";
		 	lblNewLabel.setIcon(new ImageIcon(path));
			lblNewLabel.setBounds(23, 11, 238, 152);
			add(lblNewLabel);
			
			
			MiLabel lblEmail = new MiLabel("Email",16);
			lblEmail.setBounds(274, 319, 91, 48);
			add(lblEmail);
			
			campoEmail = new MiCampoTexto(this.ventanaMadre);
			campoEmail.setBounds(379, 322, 292, 43);
			add(campoEmail);
			
			lblPassword = new MiLabel("Password",16);
			lblPassword.setBounds(236, 407, 124, 32);
			add(lblPassword);
			
			campoContrasena= new MiCampoContrasena(this.ventanaMadre);
			campoContrasena.setBounds(379, 402, 292, 43);
			add(campoContrasena);
			
			//boton aceptar ..logueo
			BotonOk boton1 = new BotonOk("Aceptar",this.ventanaMadre);
			boton1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				 
				 String email=campoEmail.getText();
				 String password=String.copyValueOf(campoContrasena.getPassword());
				 UsuarioDAO cd=null;
				try {
					cd = new UsuarioDAO(email,password);
					ventanaMadre.setUsuarioDAO(cd);//Ya tengo en ventanaMadre todos los datos del usuario y la conexion
					ventanaMadre.cargaPantallaMenuUsuario();
				} catch (MisExcepciones e1) {
					JOptionPane.showMessageDialog(ventanaMadre,e1.toString().substring(28),"Error",JOptionPane.ERROR_MESSAGE);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(ventanaMadre,"Vuelva a intentarlo mas tarde","Error",JOptionPane.ERROR_MESSAGE);
				}  
		       
				
				}
			});
			boton1.setBounds(500, 601,225, 35);
			add(boton1);
			
			//Boton atr�s..m voy a inicio
			BotonOk botonAtras = new BotonOk("Volver", (FitpointJF) null);
			botonAtras.setBounds(172,601, 225, 35);
			
			botonAtras.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					ventanaMadre.cargaPantallaInicio();
				}
	       });
			
			add(botonAtras);
			
			
		
			
		
			
			
	}
}

package interfaces;

import java.awt.Color;
/*Pantalla que se muestra cuando un usuario quiere registrarse por primera vez*/

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import componentes.BotonOk;
import componentes.MiCampoContrasena;
import componentes.MiCampoTexto;
import componentes.MiLabel;
import excepciones.MisExcepciones;
import fitpointsDAO.UsuarioDAO;

import java.awt.Dimension;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

final public class Register extends JPanel {
	private FitpointJF ventanaMadre;
	private MiCampoTexto campoNombre;
	private MiCampoTexto campoApellidos;
	private MiCampoTexto campoTelefono;
	private MiCampoTexto campoMovil;
	private MiCampoTexto campoTarjeta;
	private MiCampoTexto campoEmail;
	private MiCampoContrasena campoPassword;
	private MiCampoTexto campoDni;
	private MiCampoTexto campoCaducidad;

	
	public Register(FitpointJF fjf) {
		super();
		setSize(new Dimension(900, 700));
		setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, new Color(255, 255, 0), new Color(255, 255, 0)));
		this.ventanaMadre=fjf;
			
			setBackground(Color.WHITE);
			setLayout(null);
			
			JLabel lblNewLabel = new JLabel("");
		     String path="./src/img/fitpoints.png";
		 	lblNewLabel.setIcon(new ImageIcon(path));
			lblNewLabel.setBounds(23, 11, 238, 152);
			add(lblNewLabel);
			
			
			MiLabel lblNombre = new MiLabel("Nombre",20);
			lblNombre.setBounds(36, 189, 97, 32);
			add(lblNombre);
			
			campoNombre = new MiCampoTexto(this.ventanaMadre);
			campoNombre.setBounds(156, 184, 196, 42);
			add(campoNombre);
			campoNombre.setColumns(10);
			
			MiLabel lblApellidos = new MiLabel("Apellidos",20);
			lblApellidos.setBounds(407, 189, 121, 32);
			add(lblApellidos);
			campoApellidos = new MiCampoTexto(this.ventanaMadre);
			campoApellidos.setBounds(538, 184, 265, 42);
			add(campoApellidos);
			campoApellidos.setColumns(10);
			
			MiLabel lbltlf = new MiLabel("Telefono",20);
			lbltlf.setBounds(36, 268, 97, 32);
			add(lbltlf);
			
			campoTelefono = new MiCampoTexto(this.ventanaMadre);
			campoTelefono.setBounds(156, 263, 196, 42);
			campoTelefono.setColumns(10);
			 add(campoTelefono);
			 
			 MiLabel lblMovil = new MiLabel("Movil",20);
			 lblMovil.setBounds(443, 266, 69, 37);
			 add(lblMovil);
			 
			 campoMovil = new MiCampoTexto(this.ventanaMadre);
			 campoMovil.setBounds(538, 263, 265, 42);
			 add(campoMovil);
			 campoMovil.setColumns(10);
			 
			 MiLabel lblTarjeta = new MiLabel("Tarjeta",20);
			 lblTarjeta.setBounds(52, 347, 81, 27);
			 add(lblTarjeta);
			 
			 campoTarjeta = new MiCampoTexto(this.ventanaMadre);
			 campoTarjeta.setBounds(156, 339, 196, 42);
			 add(campoTarjeta);
			 campoTarjeta.setColumns(10);
			 
			 MiLabel lblEmail = new MiLabel("Email",20);
			 lblEmail.setBounds(64, 498, 69, 27);
			 add(lblEmail);
			 
			 campoEmail = new MiCampoTexto(this.ventanaMadre);
			 campoEmail.setBounds(156, 490, 283, 42);
			 add(campoEmail);
			 campoEmail.setColumns(10);
			 
			 MiLabel lblCaducidad = new MiLabel("Caducidad",20);
			 lblCaducidad.setBounds(399, 344, 129, 32);
			 add(lblCaducidad);
			 
			 MiLabel lbllPassword = new MiLabel("Password",20);
			 lbllPassword.setBounds(394, 411, 116, 42);
			 add(lbllPassword);
			 
			 campoPassword = new MiCampoContrasena(this.ventanaMadre);
			 campoPassword.setBounds(538, 411, 265, 42);
			 add(campoPassword);
			 campoPassword.setColumns(10);
			 
			 MiLabel lbldni = new MiLabel("DNI",20);
			 lbldni.setBounds(82, 411, 51, 42);
			 add(lbldni);
			 
			 campoDni = new MiCampoTexto(this.ventanaMadre);
			 campoDni.setBounds(156, 411, 196, 42);
			 add(campoDni);
			 campoDni.setColumns(10);
			 
			 campoCaducidad = new MiCampoTexto(this.ventanaMadre);
			 campoCaducidad.setBounds(538, 339, 265, 42);
			 add(campoCaducidad);
			 campoCaducidad.setColumns(10);
			 
			 BotonOk btnAceptar = new BotonOk("Aceptar",this.ventanaMadre);
			 btnAceptar.addMouseListener(new MouseAdapter() {
			 	@Override
			 	public void mouseClicked(MouseEvent e) {
			 		String password=String.copyValueOf(campoPassword.getPassword());
			 		//(String dni, String nombre, String apellidos,String telefono,String movil, String email, String numerotarjeta,String fecha_cad,String password,Short fit_points,ArrayList<fitpoint.Oferta>misofertas)
			 		try {
						UsuarioDAO usuario=new UsuarioDAO(campoDni.getText(),campoNombre.getText(),campoApellidos.getText(),campoTelefono.getText(),campoMovil.getText(),campoEmail.getText(),campoTarjeta.getText(),campoCaducidad.getText(),password,(short)0,null);
					    ventanaMadre.setUsuarioDAO(usuario);
					    ventanaMadre.cargaPantallaMenuUsuario();
					} catch (MisExcepciones e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(ventanaMadre, e1.toString().substring(28),"Error",JOptionPane.ERROR_MESSAGE);
					}
			 	
			 	}
			 });
						 
			 btnAceptar.setBounds(517,601,225, 35);
			 add(btnAceptar);
			 
			
			 BotonOk btnCancelar = new  BotonOk("Volver",this.ventanaMadre);
			 btnCancelar.addMouseListener(new MouseAdapter() {
			 	@Override
			 	public void mouseClicked(MouseEvent e) {
			 		ventanaMadre.cargaPantallaInicio();
			 	}
			 });
			 btnCancelar.setBounds(172,601, 225, 35);
			 add(btnCancelar);
			 
			
	
	}
}

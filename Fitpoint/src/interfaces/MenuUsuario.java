package interfaces;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import componentes.BotonBlanco;
import componentes.BotonOk;
import fitpointsDAO.UsuarioDAO;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

final public class MenuUsuario extends JPanel{
	      private FitpointJF ventanaMadre;
	      private BotonOk btnVolver;
	      private JLabel lblDatosUsuarios ;
	      
	      
	    public JLabel getLblDatosUsuarios() {
			return lblDatosUsuarios;
		}


		public void setLblDatosUsuarios(JLabel lblDatosUsuarios) {
			this.lblDatosUsuarios = lblDatosUsuarios;
		}


		public MenuUsuario(FitpointJF fjf){
	    	
	    	super();
			setSize(new Dimension(900, 700));
			setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, new Color(255, 255, 0), new Color(255, 255, 0)));
			this.ventanaMadre=fjf;
				
				setBackground(Color.WHITE);
				setLayout(null);
				
				JLabel lblNewLabel = new JLabel("");
			     String path="./src/img/fitpoints.png";
			 	lblNewLabel.setIcon(new ImageIcon(path));
				lblNewLabel.setBounds(23, 11, 238, 152);
				add(lblNewLabel);
				
				
				BotonBlanco btnTodosGimnasios = new BotonBlanco("Gimnasios",this.ventanaMadre);
				btnTodosGimnasios.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						ventanaMadre.cargaPantallaTodosGimnasios() ;
					}
				});
				btnTodosGimnasios.setBounds(53, 225, 215, 134);
				add(btnTodosGimnasios);
				
				BotonBlanco btnVerOfertas = new BotonBlanco("Ofertas",this.ventanaMadre);
				btnVerOfertas.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						ventanaMadre.cargaPantallaTodasOfertas();
					}
				});
				btnVerOfertas.setBounds(336, 225, 215, 134);
				add(btnVerOfertas);
				
				BotonBlanco btnVerMisOfertas = new BotonBlanco("Ofertas compradas",this.ventanaMadre);
				btnVerMisOfertas.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						ventanaMadre.cargaPantallaMisOfertas();
					}
				});
				btnVerMisOfertas.setBounds(53, 405, 215, 134);
				add(btnVerMisOfertas);
				
				BotonBlanco btnModificar = new BotonBlanco("Modificar mis datos",this.ventanaMadre);
				btnModificar.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						ventanaMadre.cargaPantallaModificarUsuario();
						
					}
				});
				btnModificar.setBounds(336, 405, 215, 134);
				add(btnModificar);
				
				BotonBlanco btnComprar = new BotonBlanco("Comprar Fitpoints",this.ventanaMadre);
				btnComprar.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						String msg="<html><body><p style='text-align:center;color:#7d7a86;'>"
								+"¿Cuantos fitpoints quieres comprar?</p></body></html>";
						String titulo="Compra de fitpoints";
						ComprarFitpoints dialogoError=new ComprarFitpoints(ventanaMadre,titulo,msg,true);	
					}
				});
				btnComprar.setBounds(606, 405, 215, 134);
				add(btnComprar);
				
				BotonOk btnCerrar = new BotonOk("Salir",this.ventanaMadre);
				btnCerrar.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
					//	ventanaMadre.getUsuarioDAO().getConn().close();
						ventanaMadre.dispose();
					}
				});
				btnCerrar.setBounds(342, 608, 225, 35);
				add(btnCerrar);
				
				
				BotonBlanco btnActividades = new BotonBlanco("Actividades",this.ventanaMadre);
				btnActividades.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						ventanaMadre.cargaPantallaTodasActividades();
					}
				});
				btnActividades.setBounds(606, 225, 215, 135);
				add(btnActividades);
				
				JLabel lblinstruccion = new JLabel("");
				lblinstruccion.setBounds(298, 116, 519, 49);
				add(lblinstruccion);
				
			    lblDatosUsuarios = new JLabel("");
			    //Imprime solo el nombre,apellido,numero de fitpoints y ofertas compradas actualmente
				lblDatosUsuarios.setText(ventanaMadre.getUsuarioDAO().imprimirNombreUsuario());
				lblDatosUsuarios.setBounds(302, 11, 519, 108);
				add(lblDatosUsuarios);
				
	    }
}

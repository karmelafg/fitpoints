package interfaces;
import auxiliares.*;
import javax.swing.JFrame;

import fitpointsDAO.UsuarioDAO;
/*Es la ventana principal dd luego se cargan todos los JPanel*/
final public class FitpointJF extends JFrame {
     private Inicio pantallaInicio;
     private Logear pantallaLogear;
     private Register pantallaRegister;
     private MenuUsuario pantallaMenuUsuario;
     private TodosGimnasios pantallaTodosGimnasios;
     private TodasOfertas pantallaTodasOfertas;
     private TodasActividades pantallaTodasActividades;
     private MisOfertas pantallaMisOfertas;
     private ModificarUsuario pantallaModificarUsuario;
     private FiltrarOfertas pantallaFiltrarOfertas;
     private UsuarioDAO  usuarioDAO;
     private RefrescarVentanas refrescarVentana;
     
	   public FitpointJF() {
		 super();
		 setTitle("Fitpoints");
		 this.setSize(900,700);
		
		 this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 Inicio p = new Inicio(this);
		 getContentPane().add(p);
		 setVisible(true);
		 pantallaInicio=p;
	  	}
	   
	   
	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	

	public Register getPantallaRegister() {
		return pantallaRegister;
	}
	public FiltrarOfertas getPantallaFiltrarOfertas() {
		return pantallaFiltrarOfertas;
	}
	public void setPantallaFiltrarOfertas(FiltrarOfertas pantallaFiltrarOfertas) {
		this.pantallaFiltrarOfertas = pantallaFiltrarOfertas;
	}
	public void setPantallaRegister(Register pantallaRegister) {
		this.pantallaRegister = pantallaRegister;
	}
	public TodosGimnasios getPantallaTodosGimnasios() {
		return pantallaTodosGimnasios;
	}
	public void setPantallaTodosGimnasios(TodosGimnasios pantallaTodosGimnasios) {
		this.pantallaTodosGimnasios = pantallaTodosGimnasios;
	}
	public TodasOfertas getPantallaTodasOfertas() {
		return pantallaTodasOfertas;
	}
	public void setPantallaTodasOfertas(TodasOfertas pantallaTodasOfertas) {
		this.pantallaTodasOfertas = pantallaTodasOfertas;
	}
	public TodasActividades getPantallaTodasActividades() {
		return pantallaTodasActividades;
	}
	public void setPantallaTodasActividades(TodasActividades pantallaTodasActividades) {
		this.pantallaTodasActividades = pantallaTodasActividades;
	}
	public MisOfertas getPantallaMisOfertas() {
		return pantallaMisOfertas;
	}
	public void setPantallaMisOfertas(MisOfertas pantallaMisOfertas) {
		this.pantallaMisOfertas = pantallaMisOfertas;
	}
	public ModificarUsuario getPantallaModificarUsuario() {
		return pantallaModificarUsuario;
	}
	public void setPantallaModificarUsuario(ModificarUsuario pantallaModificarUsuario) {
		this.pantallaModificarUsuario = pantallaModificarUsuario;
	}
	public MenuUsuario getPantallaMenuUsuario() {
		return pantallaMenuUsuario;
	}
	public void setPantallaMenuUsuario(MenuUsuario pantallaMenuUsuario) {
		this.pantallaMenuUsuario = pantallaMenuUsuario;
	}
	public void cargaPantallaLogear() {
		if(this.pantallaLogear==null) {
			this.pantallaLogear=new Logear(this);
		}
		this.setTitle("Fitpoints");
		this.pantallaInicio.setVisible(false);
		this.setContentPane(this.pantallaLogear);
		this.pantallaLogear.setVisible(true);
		System.out.println("Aki en logear he entrado");
	}
	public void cargaPantallaRegister() {
		if(this.pantallaRegister==null) {
			this.pantallaRegister=new Register(this);
		}
	
		this.pantallaInicio.setVisible(false);
		this.setContentPane(this.pantallaRegister);
		this.pantallaRegister.setVisible(true);
	}
	public void cargaPantallaModificarUsuario() {
		if(this.pantallaModificarUsuario==null) {
			this.pantallaModificarUsuario=new ModificarUsuario(this);
		}
		
		this.pantallaInicio.setVisible(false);
		this.setContentPane(this.pantallaModificarUsuario);
		this.pantallaModificarUsuario.setVisible(true);
	}
	public void cargaPantallaMenuUsuario() {
		if(this.pantallaLogear!=null) {
			this.pantallaLogear.setVisible(false);
		}
		if(this.pantallaRegister!=null) {
			this.pantallaRegister.setVisible(false);
		}
		if(this.pantallaTodosGimnasios!=null) {
			this.pantallaTodosGimnasios.setVisible(false);
		}
		if(this.pantallaTodasOfertas!=null) {
			this.pantallaTodasOfertas.setVisible(false);
		}
		if(this.pantallaTodasActividades!=null) {
			this.pantallaTodasActividades.setVisible(false);
		}
		if(this.pantallaModificarUsuario!=null) {
			this.pantallaModificarUsuario.setVisible(false);
		}
		if(this.pantallaMisOfertas!=null) {
			this.pantallaMisOfertas.setVisible(false);
		}
			
		if(this.pantallaMenuUsuario==null) {
			this.pantallaMenuUsuario=new MenuUsuario(this);
		}
		this.pantallaInicio.setVisible(false);
		this.setContentPane(this.pantallaMenuUsuario);
		this.pantallaMenuUsuario.setVisible(true);
	}
	public void cargaPantallaTodosGimnasios() {
		if(this.pantallaTodosGimnasios==null) {
			this.pantallaTodosGimnasios=new TodosGimnasios(this);
		}
		this.pantallaMenuUsuario.setVisible(false);
		this.setContentPane(this.pantallaTodosGimnasios);
		this.pantallaTodosGimnasios.setVisible(true);
	}
	public void cargaPantallaTodasActividades() {
		if(this.pantallaTodasActividades==null) {
		this.pantallaTodasActividades=new TodasActividades(this);
		}
		this.pantallaMenuUsuario.setVisible(false);
		this.setContentPane(this.pantallaTodasActividades);
		this.pantallaTodasActividades.setVisible(true);
	}
	public void cargaPantallaFiltrarOfertas() {
		if(this.pantallaFiltrarOfertas==null) {
		this.pantallaFiltrarOfertas=new FiltrarOfertas(this);
		}
		//this.pantallaTodasActividades.setVisible(false);
		this.setContentPane(this.pantallaFiltrarOfertas);
		this.pantallaFiltrarOfertas.setVisible(true);
	}
	public void cargaPantallaTodasOfertas() {
		if(this.pantallaTodasOfertas==null) {
			this.pantallaTodasOfertas=new TodasOfertas(this);
		}
		this.pantallaMenuUsuario.setVisible(false);
		this.setContentPane(this.pantallaTodasOfertas);
		this.pantallaTodasOfertas.setVisible(true);
	}
	public void cargaPantallaMisOfertas() {
		if(this.pantallaMisOfertas==null) {
			this.pantallaMisOfertas=new MisOfertas(this);
		}
		this.pantallaMenuUsuario.setVisible(false);
		this.setContentPane(this.pantallaMisOfertas);
		this.pantallaMisOfertas.setVisible(true);
	}
	public void cargaPantallaInicio() {
		//No hay por qu� comprobar si es nula.
		//Nunca lo va  a ser: es la pantalla por
		//defecto, y se inicializa en el constructor
		//de ventana.
		if(this.pantallaLogear!=null) {
			this.pantallaLogear.setVisible(false);
		}
		if(this.pantallaRegister!=null) {
			this.pantallaRegister.setVisible(false);
		}
		
		this.pantallaInicio.setVisible(true);
		this.setContentPane(this.pantallaInicio);
	}
	
	
}
package interfaces;


import javax.swing.JLabel;
import javax.swing.JOptionPane;

import componentes.BotonOk;
import componentes.MiLabel;
import excepciones.MisExcepciones;
import validaciones.Validacion;

import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JDialog;

import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import auxiliares.*;
final public class ComprarFitpoints extends JDialog {
	private JTextField textField;
	/*Es unJDialog que sale cuando se pulsa comprar fitpoint */
	public ComprarFitpoints(FitpointJF l,String titulo,String msg,boolean modal) {
		super(l,titulo,modal);
		getContentPane().setBackground(Color.WHITE);
		setSize(461,341);
		setBackground(new Color(250,250,250));
		getContentPane().setLayout(null);
		
		
		MiLabel lblMensaje = new MiLabel(msg,27);
		lblMensaje.setBounds(80, 29, 290, 94);
		
		lblMensaje.setForeground(new Color(125,8,52));
		lblMensaje.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lblMensaje);
		
		
		BotonOk btnCancelar = new BotonOk("Cancelar",l);
		btnCancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(257, 233, 147, 51);
		getContentPane().add(btnCancelar);
		
		textField = new JTextField();
		textField.setText("20");
		textField.setFont(new Font("Verdana", Font.PLAIN, 20));
		textField.setBounds(143, 146, 63, 48);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		BotonOk btnAceptar = new BotonOk("Confirmar",l);//Cuando se le da al boton de Confirmar-->Se compra
		btnAceptar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			  String num=textField.getText();//Compruebo que es v�lido
			  String error=Validacion.validarFitpoints(num);
			    if (error.contentEquals("")) {
			    	 int fitpoints=Integer.parseInt(num);
			          try {
						l.getUsuarioDAO().compraFitpoint(fitpoints);
					    
					     RefrescarVentanas.refresh(l);
						
					   } catch (MisExcepciones e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(l,e1,"Error",JOptionPane.ERROR_MESSAGE); 
					}
			          dispose();
				  }
				  else {
						JOptionPane.showMessageDialog(l,error,"Error",JOptionPane.ERROR_MESSAGE); 
				  }
			}
		});
		btnAceptar.setBounds(44, 231, 141, 54);
		getContentPane().add(btnAceptar);
		
		JLabel lblNewLabel = new JLabel("Fitpoints");
		lblNewLabel.setBackground(Color.white);
		lblNewLabel.setFont(new Font("Verdana", Font.PLAIN, 20));
		lblNewLabel.setBounds(209, 147, 119, 46);
		getContentPane().add(lblNewLabel);
		this.setVisible(true);
	}
}

package interfaces;

import java.awt.Color;
/**Pantalla que se ve cuando un usuario quiere modificar sus datos,sale un formulario donde se pueden modificar todos sus datos menos el dni*/
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import auxiliares.RefrescarVentanas;
import componentes.BotonOk;
import componentes.MiCampoContrasena;
import componentes.MiCampoTextLetraGris;
import componentes.MiCampoTexto;
import componentes.MiLabel;
import excepciones.MisExcepciones;
import fitpointsDAO.UsuarioDAO;

final public class ModificarUsuario extends JPanel{
	
	private FitpointJF ventanaMadre;
	private MiCampoTextLetraGris campoNombre;
	private MiCampoTextLetraGris campoApellidos;
	private MiCampoTextLetraGris campoTelefono;
	private MiCampoTextLetraGris campoMovil;
	private MiCampoTextLetraGris campoTarjeta;
	private MiCampoTextLetraGris campoEmail;
	private MiCampoContrasena campoPassword;
	private MiCampoTexto campoDni;
	private MiCampoTextLetraGris campoCaducidad;
	 private JLabel lblDatosUsuarios;
	
	public ModificarUsuario(FitpointJF fjf) {
		super();
		setSize(new Dimension(900, 700));
		setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, new Color(255, 255, 0), new Color(255, 255, 0)));
		this.ventanaMadre=fjf;
			
			setBackground(Color.WHITE);
			setLayout(null);
			
			JLabel lblNewLabel = new JLabel("");
		     String path="./src/img/fitpoints.png";
		 	lblNewLabel.setIcon(new ImageIcon(path));
			lblNewLabel.setBounds(23, 11, 238, 152);
			add(lblNewLabel);
			
			
			MiLabel lblNombre = new MiLabel("Nombre",20);
			lblNombre.setBounds(36, 189, 97, 32);
			add(lblNombre);
			
			campoNombre = new MiCampoTextLetraGris(this.ventanaMadre);
			campoNombre.setBounds(156, 184, 196, 42);
			add(campoNombre);
			campoNombre.setText(this.ventanaMadre.getUsuarioDAO().getNombre());
			campoNombre.setColumns(10);
			campoNombre.requestFocus();
			MiLabel lblApellidos = new MiLabel("Apellidos",20);
			lblApellidos.setBounds(407, 189, 121, 32);
			add(lblApellidos);
			campoApellidos = new MiCampoTextLetraGris(this.ventanaMadre);
			campoApellidos.setBounds(538, 184, 265, 42);
			campoApellidos.setText(this.ventanaMadre.getUsuarioDAO().getApellidos());
			add(campoApellidos);
			campoApellidos.setColumns(10);
			
			MiLabel lbltlf = new MiLabel("Telefono",20);
			lbltlf.setBounds(36, 268, 97, 32);
			add(lbltlf);
			
			campoTelefono = new MiCampoTextLetraGris(this.ventanaMadre);
			campoTelefono.setBounds(156, 263, 192, 42);
			campoTelefono.setText(this.ventanaMadre.getUsuarioDAO().getTelefono()+"");
			campoTelefono.setColumns(10);
			 add(campoTelefono);
			 
			 MiLabel lblMovil = new MiLabel("Movil",20);
			 lblMovil.setBounds(443, 266, 69, 37);
			 add(lblMovil);
			 
			 campoMovil = new MiCampoTextLetraGris(this.ventanaMadre);
			 campoMovil.setBounds(538, 263, 265, 42);
			 campoMovil.setText(this.ventanaMadre.getUsuarioDAO().getMovil()+"");
			 add(campoMovil);
			 campoMovil.setColumns(10);
			 
			 MiLabel lblTarjeta = new MiLabel("Tarjeta",20);
			 lblTarjeta.setBounds(52, 347, 81, 27);
			 add(lblTarjeta);
			 
			 campoTarjeta = new MiCampoTextLetraGris(this.ventanaMadre);
			 campoTarjeta.setBounds(156, 339, 192, 42);
			 campoTarjeta.setText(this.ventanaMadre.getUsuarioDAO().getNumerotarjeta());
			 add(campoTarjeta);
			 campoTarjeta.setColumns(10);
			 
			 MiLabel lblEmail = new MiLabel("Email",20);
			 lblEmail.setBounds(64, 507, 69, 27);
			 add(lblEmail);
			 
			 campoEmail = new MiCampoTextLetraGris(this.ventanaMadre);
			 campoEmail.setBounds(153, 499, 297, 42);
			 campoEmail.setText(this.ventanaMadre.getUsuarioDAO().getEmail());
			 add(campoEmail);
			 campoEmail.setColumns(10);
			 
			 MiLabel lblCaducidad = new MiLabel("Caducidad",20);
			 lblCaducidad.setBounds(399, 344, 129, 32);
			 add(lblCaducidad);
			 
			 MiLabel lbllPassword = new MiLabel("Password",20);
			 lbllPassword.setBounds(394, 411, 116, 42);
			 add(lbllPassword);
			 
			 campoPassword = new MiCampoContrasena(this.ventanaMadre);
			 campoPassword.setBounds(538, 411, 265, 42);
			 campoPassword.setText(this.ventanaMadre.getUsuarioDAO().getPassword());
			 add(campoPassword);
			 campoPassword.setColumns(10);
			 
			 MiLabel lbldni = new MiLabel("DNI",20);
			 lbldni.setBounds(82, 411, 51, 42);
			 add(lbldni);
			 
			 campoDni = new MiCampoTexto(this.ventanaMadre);
			 campoDni.setBounds(156, 411, 196, 42);
			 campoDni.setEnabled(false);
			 campoDni.setForeground(Color.BLACK);
			 campoDni.setText(this.ventanaMadre.getUsuarioDAO().getDni());
			 add(campoDni);
			 campoDni.setColumns(10);
			 
			 campoCaducidad = new MiCampoTextLetraGris(this.ventanaMadre);
			 campoCaducidad.setBounds(538, 339, 265, 42);
			 campoCaducidad.setText(this.ventanaMadre.getUsuarioDAO().getFecha_cad()+"");
			 add(campoCaducidad);
			 campoCaducidad.setColumns(10);
			 
			 BotonOk btnAceptar = new BotonOk("Aceptar",this.ventanaMadre);
			 btnAceptar.addMouseListener(new MouseAdapter() {
			 	@Override
			 	public void mouseClicked(MouseEvent e) {
			 		String password=String.copyValueOf(campoPassword.getPassword());
			 		//(String dni, String nombre, String apellidos,String telefono,String movil, String email, String numerotarjeta,String fecha_cad,String password,Short fit_points,ArrayList<fitpoint.Oferta>misofertas)
			 		try {
			 			
						ventanaMadre.getUsuarioDAO().modificarUsuario(campoNombre.getText(), campoApellidos.getText(), campoTelefono.getText(), campoMovil.getText(), campoEmail.getText(),campoTarjeta.getText(), campoCaducidad.getText(), password);
						//ventanaMadre.getPantallaMenuUsuario().getLblDatosUsuarios().setText(ventanaMadre.getUsuarioDAO().imprimirNombreUsuario());
						RefrescarVentanas.refresh(ventanaMadre);
						ventanaMadre.cargaPantallaMenuUsuario();
					} catch (MisExcepciones e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(ventanaMadre, e1.toString().substring(28),"Error",JOptionPane.ERROR_MESSAGE);
					}
			 	
			 	}
			 });
						 
			 btnAceptar.setBounds(517, 601,225, 35);
			 add(btnAceptar);
			 
			
			 BotonOk btnCancelar = new  BotonOk("Volver",this.ventanaMadre);
			 btnCancelar.addMouseListener(new MouseAdapter() {
			 	@Override
			 	public void mouseClicked(MouseEvent e) {
			 		ventanaMadre.cargaPantallaMenuUsuario();
			 	}
			 });
			 btnCancelar.setBounds(172, 601, 225, 35);
			 add(btnCancelar);
			 
			 lblDatosUsuarios = new JLabel("");
				lblDatosUsuarios.setText(ventanaMadre.getUsuarioDAO().imprimirNombreUsuario());
				
				add(lblDatosUsuarios);
				JLabel lblinstruccion = new JLabel("");
				lblinstruccion.setText("<html><body><p style='text-align:left;font-family:verdana;font-size:13px;color:#7d7a86;'>Modifica tus datos</p></body></html>");	
				lblinstruccion.setBounds(298, 119, 519, 45);
				lblDatosUsuarios.setBounds(298, 11, 519, 102);
				add(lblinstruccion);
			
			 
			
	
	}

	public JLabel getLblDatosUsuarios() {
		return lblDatosUsuarios;
	}

	public void setLblDatosUsuarios(JLabel lblDatosUsuarios) {
		this.lblDatosUsuarios = lblDatosUsuarios;
	}
}

	
	
	
	


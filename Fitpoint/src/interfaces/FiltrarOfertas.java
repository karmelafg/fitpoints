package interfaces;

/*Pantalla que sale cuando el usuario quiere filtrar las ofertas por fecha,actividad o gimnasio*/
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.plaf.basic.BasicScrollBarUI;
import auxiliares.RefrescarVentanas;
import componentes.BotonOk;
import componentes.MiComboBox;
import excepciones.MisExcepciones;
import fitpoint.ActividadCombo;
import fitpoint.GimnasioCombo;
import fitpointsDAO.GimnasioDAO;
import fitpointsDAO.OfertaDAO;
import fitpointsDAO.UsuarioDAO;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;

import javax.swing.Box;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JDayChooser;

import javax.swing.border.LineBorder;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import javax.swing.JCheckBox;

final public class FiltrarOfertas extends JPanel {
	private FitpointJF ventanaMadre;
	private JEditorPane editorPane;
	private JLabel lblDatosUsuarios;
	private JList list ;
	private MiComboBox cbGim;
	private MiComboBox cbActiv;
    private ArrayList<OfertaDAO> miarrayOferta;
    private JCalendar calendario;
    public String fechaElegida="0"; //0-->si lo cojo del checkbox..todos las fechas,1-->Si lo cojo delJCalendar
    private JCheckBox checkBox;
   
   
   
	public String getFechaElegida() {
		return fechaElegida;
	}



	public void setFechaElegida(String fechaElegida) {
		this.fechaElegida = fechaElegida;
	}



	public FiltrarOfertas(FitpointJF fjf) {
		super();	
		this.ventanaMadre=fjf;
		setSize(new Dimension(900, 700));
		setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, new Color(255, 255, 0), new Color(255, 255, 0)));	
		setBackground(Color.WHITE);
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
	     String path="./src/img/fitpoints.png";
	 	lblNewLabel.setIcon(new ImageIcon(path));
		lblNewLabel.setBounds(23, 11, 238, 152);
		add(lblNewLabel);
		lblDatosUsuarios = new JLabel("");
		lblDatosUsuarios.setText(ventanaMadre.getUsuarioDAO().imprimirNombreUsuario());
		lblDatosUsuarios.setBounds(298, 11, 519, 102);
		add(lblDatosUsuarios);
		JLabel lblinstruccion = new JLabel("");
		lblinstruccion.setText("<html><body><p style='text-align:left;font-family:verdana;font-size:13px;color:#7d7a86;'>Filtra las ofertas disponibles, si alguna te interesa s�lo tienes que clickear en ella y comprarla</p></body></html>");	
		lblinstruccion.setBounds(298, 119, 519, 45);
		lblDatosUsuarios.setBounds(298, 11, 519, 102);
		add(lblinstruccion);
	
		BotonOk btnAtras = new BotonOk("Cancelar",this.ventanaMadre);
		btnAtras.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				inicializar(ventanaMadre);
				ventanaMadre.cargaPantallaMenuUsuario();
			}
		});
		btnAtras.setBounds(342, 608, 225, 35);
		add(btnAtras);
		
		//El objeto List en un principio con todas las ofertas actuales
		//Luego segun los valores de los filtros ir�n variando..
		  ArrayList<OfertaDAO> arrayOferta= new ArrayList<OfertaDAO> ();
		  DefaultListModel arrayOfertaString = new DefaultListModel<>();
		 try {
			 arrayOferta=ventanaMadre.getUsuarioDAO().devolverTodasLasOfertas();
			 this.miarrayOferta=arrayOferta;
			 if (!arrayOferta.isEmpty()) {//Para imprimir las ofertas en el List
				
				 for (int i=0; i<arrayOferta.size();i++){ ///Para que sólo saque las actuales
					   arrayOfertaString.addElement(arrayOferta.get(i).imprimirOferta());  
	             } 
			 }else {
				 String msj="<html><body><table width='580px' style='margin-bottom:2px;'><tr style='background-color:#f2f2f2;border-left:3px dotted #fbe391'>"
						 +"<td style='width:100%;'><p style='font-size:13px;font-family:verdana;text-align=center;'><i>"
						 +"No existen ofertas con esos par�metros de busqueda elegidos"+
			                "</i></p></td></tr>"+"</table></body></html>";
				  arrayOfertaString.addElement(msj);  
			 }
		} catch (MisExcepciones e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(ventanaMadre,e.getMessage(),"Informacion",JOptionPane.INFORMATION_MESSAGE);
			
			e.printStackTrace();
			System.out.println("Error en devolver todas las ofertas");
		} 
		    int indice=-1;
			list = new JList();
			
			list.setModel(arrayOfertaString);
			list.setBounds(58, 218, 580, 200);
			list.setFont(new Font("Verdana", Font.PLAIN, 12));
			list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		
			list.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (!miarrayOferta.isEmpty()) {
					int indice=list.getSelectedIndex();
					int seleccion = JOptionPane.showOptionDialog(
							   null,
							   "�Esta segur@ de comprar esta oferta?", 
							   "Compra de oferta",
							   JOptionPane.YES_NO_CANCEL_OPTION,
							   JOptionPane.QUESTION_MESSAGE,
							   null,  
							   new Object[] { "Confirmar", "Cancelar"},   // null para YES, NO y CANCEL
							   "Confirmar");

					 if (seleccion == 0) {
				      try {
							ventanaMadre.getUsuarioDAO().comprar(miarrayOferta.get(indice).getId_oferta());
							//ventanaMadre.getPantallaMenuUsuario().getLblDatosUsuarios().setText(ventanaMadre.getUsuarioDAO().imprimirNombreUsuario());
							  RefrescarVentanas.refresh(ventanaMadre);
							ventanaMadre.cargaPantallaMenuUsuario();
						} catch (MisExcepciones e1) {
							JOptionPane.showMessageDialog(ventanaMadre, e1.toString().substring(28),"Error",JOptionPane.ERROR_MESSAGE);
						}
				  	 }else {
				  		 list.setSelectedIndex(-1);
				  		 
				  	 }
					}
				}});
		    list.setSelectionBackground(new Color(251,227,145)); 
			list.setVisible(true);
			add(list);
			JScrollPane scroll = new JScrollPane(list);
			
			scroll.setSize(new Dimension(796, 200));
		 	scroll.setLocation(new Point(35, 197));
			scroll.setVerticalScrollBarPolicy(scroll.VERTICAL_SCROLLBAR_AS_NEEDED);
			scroll.setHorizontalScrollBarPolicy(scroll.HORIZONTAL_SCROLLBAR_NEVER);
			scroll.setBorder(null);
			
		
	      	 scroll.getVerticalScrollBar().setUI(new BasicScrollBarUI()
			 {   
			        @Override
			        protected JButton createDecreaseButton(int orientation) {
			            return createZeroButton();
			        }

			        @Override    
			        protected JButton createIncreaseButton(int orientation) {
			            return createZeroButton();
			        }

			        private JButton createZeroButton() {
			            JButton jbutton = new JButton();
			            jbutton.setPreferredSize(new Dimension(0, 0));
			            jbutton.setMinimumSize(new Dimension(0, 0));
			            jbutton.setMaximumSize(new Dimension(0, 0));
			            return jbutton;
			        }
			        @Override 
			        protected void configureScrollBarColors(){
			            this.thumbColor = Color.white;
			            this.thumbLightShadowColor=new Color(251,227,145);
			            this.thumbDarkShadowColor=new Color(251,227,145);
			            this.thumbHighlightColor=new Color(251,227,145);
			         
			        }
			 });
			add(scroll);
			
			//El primer combobox que es para filtrar por gimanasio
			cbGim = new MiComboBox(fjf);
			cbGim.setBounds(511, 553,300,30);
			cbGim.setBorder(new LineBorder(Color.getColor("Micolor",new Color(251,227,145)),2, true));
			cbGim.setBackground(Color.WHITE);
			cbGim.setForeground(Color.GRAY);
			
		    cargarComboBox(cbGim,ventanaMadre,true);
			add(cbGim);
			scroll.setVisible(true);
			cbGim.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
				   String fecha;
					if (fechaElegida.equals("1")) {
					 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");			
					fecha=format.format(calendario.getDate());
				  }else {
					fecha="0";
				 }
				 GimnasioCombo gim = (GimnasioCombo) cbGim.getSelectedItem() ;
				  ActividadCombo act=(ActividadCombo) cbActiv.getSelectedItem();
				  //Esta funcion le paso el JList y los para�metros de filtro y cambia el Jlist con esa consulta
				 //Ademas pone devuelve un Arraylist<OfertaDAO> que se lo asignaremos a miArrayOferta que es un atributo de este JPAnel
				  miarrayOferta=cargarFiltradosLista(list,fjf,gim.getID(),act.getID(),fecha); 
				 // System.out.println(gim.getID());
				}

				
			});
			
			//El segundo comobobosx para filtrar por ofertas
			cbActiv = new MiComboBox(fjf);
			cbActiv.setBounds(511, 502,300,35);
			cbActiv.setBorder(new LineBorder(Color.getColor("Micolor",new Color(251,227,145)),2, true));
			cbActiv.setBackground(Color.WHITE);
			cbActiv.setForeground(Color.GRAY);
		    cargarComboBox(cbActiv,ventanaMadre,false);
			add(cbActiv);
			cbActiv.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
				   String fecha;
					if (fechaElegida.equals("1")) {
					 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");			
					fecha=format.format(calendario.getDate());
				  }else {
					fecha="0";
				 }
				 GimnasioCombo gim = (GimnasioCombo) cbGim.getSelectedItem() ;
				  ActividadCombo act=(ActividadCombo) cbActiv.getSelectedItem();
				  //Esta funcion le paso el JList y los para�metros de filtro y cambia el Jlist con esa consulta
				 //Ademas pone devuelve un Arraylist<OfertaDAO> que se lo asignaremos a miArrayOferta que es un atributo de este JPAnel
				  miarrayOferta=cargarFiltradosLista(list,fjf,gim.getID(),act.getID(),fecha); 
				 // System.out.println(gim.getID());
				}

				
			});
			//El calendario
			
			calendario = new JCalendar();
			calendario.setBounds(58, 451, 300, 140);
			calendario.setWeekOfYearVisible(false);
			calendario.setMaxDayCharacters(1);
			calendario.setForeground(Color.DARK_GRAY);
			calendario.setWeekdayForeground(new Color(203, 158, 7));
		//	calendario.setSundayForeground(new Color(203, 158, 7));
			calendario.setBorder(new LineBorder(Color.getColor("Micolor",new Color(251,227,145)),2, true));
			calendario.setEnabled(false);	  
		   	
				 
			 calendario.getDayChooser().addPropertyChangeListener("day",new PropertyChangeListener() {
				 @Override
				 public void propertyChange(PropertyChangeEvent evt) {
			             	fechaElegida="1";
			             	setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			             	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");			
			            	String fecha=format.format(calendario.getDate());
			            	LocalDate fn=LocalDate.parse(fecha);
			                    if (fn.isBefore(LocalDate.now())) {
			                    	JOptionPane.showMessageDialog(ventanaMadre,"Error:La fecha debe ser posterior a hoy","Error",JOptionPane.WARNING_MESSAGE);
			                    }else {
					                GimnasioCombo gim = (GimnasioCombo) cbGim.getSelectedItem() ;
									ActividadCombo acti= (ActividadCombo) cbActiv.getSelectedItem() ;
									miarrayOferta=cargarFiltradosLista(list,fjf,gim.getID(),acti.getID(),fecha); 
			                    }
			               
			     }
            });
			calendario.getMonthChooser().addPropertyChangeListener("month",new PropertyChangeListener() {
				 @Override
				 public void propertyChange(PropertyChangeEvent evt) {
			             	fechaElegida="1";
			             	setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			             	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");			
			            	String fecha=format.format(calendario.getDate());
			            	LocalDate fn=LocalDate.parse(fecha);
			                    if (fn.isBefore(LocalDate.now())) {
			                    	JOptionPane.showMessageDialog(ventanaMadre,"Error:La fecha debe ser posterior a hoy","Error",JOptionPane.WARNING_MESSAGE);
			                    }else {
					                GimnasioCombo gim = (GimnasioCombo) cbGim.getSelectedItem() ;
									ActividadCombo acti= (ActividadCombo) cbActiv.getSelectedItem() ;
									miarrayOferta=cargarFiltradosLista(list,fjf,gim.getID(),acti.getID(),fecha); 
			                    }
			               
			     }
            });
			calendario.getYearChooser().addPropertyChangeListener("year",new PropertyChangeListener() {
				 @Override
				 public void propertyChange(PropertyChangeEvent evt) {
			             	fechaElegida="1";
			             	setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			             	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");			
			            	String fecha=format.format(calendario.getDate());
			            	LocalDate fn=LocalDate.parse(fecha);
			                    if (fn.isBefore(LocalDate.now())) {
			                    	JOptionPane.showMessageDialog(ventanaMadre,"Error:La fecha debe ser posterior a hoy","Error",JOptionPane.WARNING_MESSAGE);
			                    }else {
					                GimnasioCombo gim = (GimnasioCombo) cbGim.getSelectedItem() ;
									ActividadCombo acti= (ActividadCombo) cbActiv.getSelectedItem() ;
									miarrayOferta=cargarFiltradosLista(list,fjf,gim.getID(),acti.getID(),fecha); 
			                    }
			               
			     }
           });
			add(calendario);
			//Un checkbox para ver todas las fechas..cuando st� activa se deshabilita el Jcalendar y viceversa
		    checkBox = new JCheckBox("Todas las fechas");
		    checkBox.setSelected(true);
		    checkBox.setBackground(Color.WHITE);
		    checkBox.setForeground(Color.GRAY);
		    checkBox.setBorder(new LineBorder(Color.getColor("Micolor",new Color(251,227,145)),2, true));
			checkBox.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					GimnasioCombo gim = (GimnasioCombo) cbGim.getSelectedItem() ;
					ActividadCombo acti= (ActividadCombo) cbActiv.getSelectedItem() ;
					String fecha;
					if (checkBox.isSelected()) {
						calendario.setEnabled(false);
						fechaElegida="0";
						fecha="0";
					}else {
						Calendar c2=new GregorianCalendar();
						calendario.setCalendar(c2); 
						calendario.setEnabled(true);
						 fechaElegida="1";
					     SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");			
			             fecha=format.format(calendario.getDate());
						
						
					}
					 miarrayOferta=cargarFiltradosLista(list,fjf,gim.getID(),acti.getID(),fecha); 
					
				}
			});
			
			
			
			
			checkBox.setFont(new Font("Verdana", Font.PLAIN, 20));
			checkBox.setBounds(511, 451, 300, 35);
			add(checkBox);
			
	}
					
			
	/**Carga en el arryList de esta pantalla los datos de las ofertas filtradas por iddim,idactividad y fecha
	 * 
	 * 
	 * @param lista 
	 * @param ventanaMadre
	 * @param idgimnasio
	 * @param idactividad
	 * @param fecha vale="0"-->Si keremos q salgan todas las fechas..o una fecha en String si keremos una oferta concreta
	 * @return
	 */
	
	public static  ArrayList<OfertaDAO> cargarFiltradosLista(JList lista,FitpointJF ventanaMadre,int idgimnasio,int idactividad,String fecha) {
	     lista.setVisible(true);
		 lista.removeAll();
		 lista.setEnabled(true);
		
		
	   ArrayList<OfertaDAO> arrayOferta= new ArrayList<OfertaDAO> ();
	   DefaultListModel arrayOfertaString = new DefaultListModel<>();
	  //Paso
	   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");			
       java.util.Date date=null;
       
       String fecha1=ventanaMadre.getPantallaFiltrarOfertas().getFechaElegida();
          		 try {
				 arrayOferta=ventanaMadre.getUsuarioDAO().devolverOfertasfiltradas(idgimnasio,idactividad,fecha);
					 if (!arrayOferta.isEmpty()) {//Para imprimir las ofertas en el List
						 for (int i=0; i<arrayOferta.size();i++){ ///Para que sólo saque las actuales
							   arrayOfertaString.addElement(arrayOferta.get(i).imprimirOferta());  
			             } 
						
					 }else {//En el caso que no se cumpla ningun par�metro de b�queda,inicializamos para qu de todos los resultados
						 String msj="<html><body><table width='580px' style='margin-bottom:2px;'><tr style='background-color:#f2f2f2;border-left:3px dotted #fbe391'>"
								 +"<td style='width:100%;'><p style='font-size:13px;font-family:verdana;text-align=center;'><i>"
								 +"No existen ofertas con esos par�metros de busqueda elegidos"+
					                "</i></p></td></tr>"+"</table></body></html>";
						 arrayOfertaString.addElement(msj);  
									
					 }
						 
				  } catch (MisExcepciones e) {
					 JOptionPane.showMessageDialog(ventanaMadre,e.getMessage(),"Informacion",JOptionPane.INFORMATION_MESSAGE);
				
				 } 
          		lista.setModel(arrayOfertaString);
        	    return arrayOferta;
    	   
       }
		
 	
		
	
	
	
	
	
	/**Funcion que sirve tanto para rellenar tanto el Combo de las actividades como el de los gimnasios
	 * Si comGim==true-->Hay que rellenarlo con todos los gimansios que hay en la BBDD en el comboGym
	 * Si es false -->Hay que rellenarlo con tods las ofertas de la BBDD en el comboActi
	 * @param cb el combobox en si
	 * @param ventanaMadre 
	 * @param comboGim la variable booleana
	 */
	public static void cargarComboBox(JComboBox cb,FitpointJF ventanaMadre,boolean comboGim) {
		cb.removeAllItems();
		PreparedStatement smt=null;
		ResultSet rs=null;
		DefaultComboBoxModel value=new DefaultComboBoxModel();
		cb.setModel(value);
		String nombre="";
		int id=-1;
		try {
			if (comboGim) { 
				smt = ventanaMadre.getUsuarioDAO().getConn().prepareStatement("select id_gimansio,nombre from fitpoints3.gimnasio ");
		         rs=smt.executeQuery();
		         value.addElement(new GimnasioCombo("--Todos los gimnasios--",0));
		         while (rs.next()) {
					 nombre=rs.getString("nombre");
					 id=rs.getInt("id_gimansio");
					 value.addElement(new GimnasioCombo(nombre,id));
					// System.out.println("el inice es:" +id);																
					
				}
			} else {
				smt = ventanaMadre.getUsuarioDAO().getConn().prepareStatement("select id_actividad,nombre from fitpoints3.actividad ");
		         rs=smt.executeQuery();
		         value.addElement(new ActividadCombo("--Todas las actividades--",0));
		         while (rs.next()) {
					 nombre=rs.getString("nombre");
					 id=rs.getInt("id_actividad");
					 value.addElement(new ActividadCombo(nombre,id));
					 System.out.println("el inice es:" +id);																
					
				}
				
			}
			cb.setModel(value);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(ventanaMadre,e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
		}finally {
			  try { 
                  if (rs!=null){ 
                     rs.close();
                  }
                   if (smt!=null){ 
                      smt.close();
                  }
                  
                } catch (SQLException ex) {
                	JOptionPane.showMessageDialog(ventanaMadre,"Error al cerrar la Base de Datos","Error",JOptionPane.ERROR_MESSAGE);
                }
		}
			
	}
	public void inicializar(FitpointJF ventanaMadre) {
		Calendar c2=new GregorianCalendar();
		ventanaMadre.getPantallaFiltrarOfertas().calendario.setCalendar(c2);
		ventanaMadre.getPantallaFiltrarOfertas().calendario.setEnabled(false);
		ventanaMadre.getPantallaFiltrarOfertas().fechaElegida="0";
		ventanaMadre.getPantallaFiltrarOfertas().cbActiv.setSelectedIndex(0);
		ventanaMadre.getPantallaFiltrarOfertas().cbGim.setSelectedIndex(0);
		ventanaMadre.getPantallaFiltrarOfertas().checkBox.setSelected(true);
		
	}
	 public JLabel getLblDatosUsuarios() {
		return lblDatosUsuarios;
	 }
	 public void setLblDatosUsuarios(JLabel lblDatosUsuarios) {
  		this.lblDatosUsuarios = lblDatosUsuarios;
	 }
	
	
	}



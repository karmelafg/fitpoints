package interfaces;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;

import componentes.BotonBlanco;
import componentes.BotonOk;


import javax.swing.border.BevelBorder;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;

final public class Inicio extends JPanel {
	private FitpointJF ventanaMadre;
	/**
	 * @wbp.nonvisual location=418,373
	 */


	/** Ventana de inicio que d� a elegir entre logear y registrarse
	 * 
	 */
	public Inicio(FitpointJF fjf) {
		super();
		this.ventanaMadre=fjf;
		setBackground(Color.WHITE);
		setLayout(null);
		JLabel lblNewLabel = new JLabel("");
	    String path="./src/img/fitpoints.png";
	 	lblNewLabel.setIcon(new ImageIcon(path));
		lblNewLabel.setBounds(23, 11, 238, 152);
		add(lblNewLabel);
		
		BotonBlanco botonReg = new BotonBlanco("Registrarse",this.ventanaMadre);
		
		botonReg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.out.println("Aqui entra bien");
				ventanaMadre.cargaPantallaRegister();
			}
          });
		botonReg.setBounds(80, 325, 321,185);
		add(botonReg);
		
		BotonOk botonSalir = new BotonOk("Salir",this.ventanaMadre);
		botonSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ventanaMadre.dispose();
			}
		});
		botonSalir.setBounds(342, 608, 225, 35);
		add(botonSalir);
		
		BotonBlanco botonLogear = new BotonBlanco("Logearse",this.ventanaMadre);
		//Si le da la boton logearse pasa a la siguiente pantalla
		botonLogear.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						ventanaMadre.cargaPantallaLogear();
					}
		});
		
		botonLogear.setBounds(480, 325, 321, 185);
		add(botonLogear);
		
		
	}
}

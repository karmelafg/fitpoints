package interfaces;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.plaf.basic.BasicScrollBarUI;

import componentes.BotonOk;
import excepciones.MisExcepciones;
//Pantalla que se muestra cuando el usuario quiere ver las ofertas que ya ha comprado
final public class MisOfertas extends JPanel {
	private FitpointJF ventanaMadre;
	 private JLabel lblDatosUsuarios;
	 private JEditorPane editorPane;
	public MisOfertas(FitpointJF fjf) {
		super();
		this.ventanaMadre=fjf;
		setSize(new Dimension(900, 700));
		setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, new Color(255, 255, 0), new Color(255, 255, 0)));	
		setBackground(Color.WHITE);
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
	     String path="./src/img/fitpoints.png";
	 	lblNewLabel.setIcon(new ImageIcon(path));
		lblNewLabel.setBounds(23, 11, 238, 152);
		add(lblNewLabel);
		
		
		 editorPane = new JEditorPane();
		//editorPane.setBounds(10, 197, 800, 392);
		//add(editorPane);
		editorPane.setContentType("text/html"); 
		editorPane.setEditable(false);
		
		String cadena="<html><body>";
		
		try {
			
			String cadena1=ventanaMadre.getUsuarioDAO().imprimirMisOfertas();
			cadena=cadena+cadena1;
		} catch (MisExcepciones e) {
			JOptionPane.showMessageDialog(ventanaMadre, e.toString().substring(28),"Error",JOptionPane.ERROR_MESSAGE);
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(ventanaMadre, "Intentelo mas tarde..","Error",JOptionPane.ERROR_MESSAGE);
		}
		
		
		cadena=cadena+"</body></html>";
		//System.out.println(cadena);
		editorPane.setText(cadena);
		editorPane.setVisible(true);
	 	JScrollPane scroll=new JScrollPane(editorPane);
	 	scroll.setSize(new Dimension(800, 392));
	 	scroll.setLocation(new Point(35, 197));
	 	scroll.setVerticalScrollBarPolicy(scroll.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setBorder(null);
		
		/**********************************************/
		scroll.getVerticalScrollBar().setUI(new BasicScrollBarUI()
		 {   
		        @Override
		        protected JButton createDecreaseButton(int orientation) {
		            return createZeroButton();
		        }

		        @Override    
		        protected JButton createIncreaseButton(int orientation) {
		            return createZeroButton();
		        }

		        private JButton createZeroButton() {
		            JButton jbutton = new JButton();
		            jbutton.setPreferredSize(new Dimension(0, 0));
		            jbutton.setMinimumSize(new Dimension(0, 0));
		            jbutton.setMaximumSize(new Dimension(0, 0));
		            return jbutton;
		        }
		        @Override 
		        protected void configureScrollBarColors(){
		            this.thumbColor = Color.white;
		            this.thumbLightShadowColor=new Color(251,227,145);
		            this.thumbDarkShadowColor=new Color(251,227,145);
		            this.thumbHighlightColor=new Color(251,227,145);
		         
		        }
		    });
		scroll.setBorder(null);
		add(scroll);
		scroll.setVisible(true);
		 lblDatosUsuarios = new JLabel("");
		lblDatosUsuarios.setText(ventanaMadre.getUsuarioDAO().imprimirNombreUsuario());
		
		add(lblDatosUsuarios);
		
		JLabel lblinstruccion = new JLabel("");
		lblinstruccion.setText("<html><body><p style='text-align:left;font-family:verdana;font-size:14px;color:#7d7a86;'>Ofertas que tienes compradas</p></body></html>");
		lblinstruccion.setBounds(298, 119, 519, 45);
		lblDatosUsuarios.setBounds(298, 11, 519, 102);
		add(lblinstruccion);
		
		BotonOk btnAtras = new BotonOk("Volver",this.ventanaMadre);
		btnAtras.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ventanaMadre.cargaPantallaMenuUsuario();
			}
		});
		btnAtras.setBounds(342, 608, 225, 35);
		add(btnAtras);
		
		



		

}
	
	public JEditorPane getEditorPane() {
		return editorPane;
	}

	public void setEditorPane(JEditorPane editorPane) {
		this.editorPane = editorPane;
	}

	public JLabel getLblDatosUsuarios() {
		return lblDatosUsuarios;
	}
	public void setLblDatosUsuarios(JLabel lblDatosUsuarios) {
		this.lblDatosUsuarios = lblDatosUsuarios;
	}
}
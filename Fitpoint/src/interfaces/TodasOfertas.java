package interfaces;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.plaf.basic.BasicScrollBarUI;

import auxiliares.RefrescarVentanas;
import componentes.BotonOk;
import excepciones.MisExcepciones;
import fitpoint.Fitpoint;
import fitpointsDAO.OfertaDAO;


import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
/*Pantalla que se muestra cuando un usuario quiere ver todas las ofertas actuales y si clickea sobre una de ellas puede comprarla directamente.
 * Adem�s tiene un bot�n de filtrar que da la posibilidad de filtrar las ofertas*/
final public class TodasOfertas extends JPanel {
 private FitpointJF ventanaMadre;
 private ArrayList<OfertaDAO> miarrayOferta;
 private JLabel lblDatosUsuarios;
  public TodasOfertas(FitpointJF fjf) {
		super();
		this.ventanaMadre=fjf;
		setSize(new Dimension(900, 700));
		setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, new Color(255, 255, 0), new Color(255, 255, 0)));	
		setBackground(Color.WHITE);
		setLayout(null);
		JLabel lblNewLabel = new JLabel("");
	     String path="./src/img/fitpoints.png";
	 	lblNewLabel.setIcon(new ImageIcon(path));
		lblNewLabel.setBounds(23, 11, 238, 152);
		add(lblNewLabel);
		
		ArrayList<OfertaDAO> arrayOferta= new ArrayList<OfertaDAO> ();
		  DefaultListModel arrayOfertaString = new DefaultListModel<>();
		 try {
			 arrayOferta=ventanaMadre.getUsuarioDAO().devolverTodasLasOfertas();
			 this.miarrayOferta=arrayOferta;
			 if (!arrayOferta.isEmpty()) {
				// System.out.println("Justo despues del bucle");
				 for (int i=0; i<arrayOferta.size();i++){ ///Para que sólo saque las actuales
					   arrayOfertaString.addElement(arrayOferta.get(i).imprimirOferta());  
	             } 
			 }else {
				String msj="<html><body><table width='580px' style='margin-bottom:2px;'><tr style='background-color:#f2f2f2;border-left:3px dotted #fbe391'>"
						 +"<td style='width:100%;'><p style='font-size:13px;font-family:verdana;text-align=center;'><i>"
						 +"No existen ofertas con esos par�metros de busqueda elegidos"+
			                "</i></p></td></tr>"+"</table></body></html>";
				  arrayOfertaString.addElement(msj);  
			 }
		} catch (MisExcepciones e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		    int indice=-1;
			JList list = new JList();
			
			list.setModel(arrayOfertaString);
			list.setBounds(58, 218, 580, 385);
			list.setFont(new Font("Verdana", Font.PLAIN, 12));
			list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		
			list.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (!miarrayOferta.isEmpty()) {
						int indice=list.getSelectedIndex();
						int seleccion = JOptionPane.showOptionDialog(
								   null,
								   "�Esta segur@ de comprar esta oferta?", 
								   "Compra de oferta",
								   JOptionPane.YES_NO_CANCEL_OPTION,
								   JOptionPane.QUESTION_MESSAGE,
								   null,  
								   new Object[] { "Confirmar", "Cancelar"},   // null para YES, NO y CANCEL
								   "Confirmar");
	
						 if (seleccion == 0) {
					      try {
					    	    int ind=miarrayOferta.get(indice).getId_oferta();
								ventanaMadre.getUsuarioDAO().comprar(ind);
								crearArchivo(ind,ventanaMadre) ;
								//ventanaMadre.getPantallaMenuUsuario().getLblDatosUsuarios().setText(ventanaMadre.getUsuarioDAO().imprimirNombreUsuario());
								  
								RefrescarVentanas.refresh(ventanaMadre);
								  
								  
								  
								  
								ventanaMadre.cargaPantallaMenuUsuario();
							} catch (MisExcepciones e1) {
								JOptionPane.showMessageDialog(ventanaMadre, e1.toString().substring(28),"Error",JOptionPane.ERROR_MESSAGE);
							}
					  	 }else {
					  		 list.setSelectedIndex(-1);
					  		 
					  	 }
					}	 
				}});
		    list.setSelectionBackground(new Color(251,227,145)); 
			list.setVisible(true);
			add(list);
			JScrollPane scroll = new JScrollPane(list);
			
			scroll.setSize(new Dimension(796, 385));
		 	scroll.setLocation(new Point(35, 197));
			scroll.setVerticalScrollBarPolicy(scroll.VERTICAL_SCROLLBAR_AS_NEEDED);
			scroll.setHorizontalScrollBarPolicy(scroll.HORIZONTAL_SCROLLBAR_NEVER);
			scroll.setBorder(null);
			
			/**********************************************/
	  	 scroll.getVerticalScrollBar().setUI(new BasicScrollBarUI()
			 {   
			        @Override
			        protected JButton createDecreaseButton(int orientation) {
			            return createZeroButton();
			        }

			        @Override    
			        protected JButton createIncreaseButton(int orientation) {
			            return createZeroButton();
			        }

			        private JButton createZeroButton() {
			            JButton jbutton = new JButton();
			            jbutton.setPreferredSize(new Dimension(0, 0));
			            jbutton.setMinimumSize(new Dimension(0, 0));
			            jbutton.setMaximumSize(new Dimension(0, 0));
			            return jbutton;
			        }
			        @Override 
			        protected void configureScrollBarColors(){
			            this.thumbColor = Color.white;
			            this.thumbLightShadowColor=new Color(251,227,145);
			            this.thumbDarkShadowColor=new Color(251,227,145);
			            this.thumbHighlightColor=new Color(251,227,145);
			         
			        }
			    });
			add(scroll);
			scroll.setVisible(true);
			lblDatosUsuarios = new JLabel("");
			lblDatosUsuarios.setText(ventanaMadre.getUsuarioDAO().imprimirNombreUsuario());
			lblDatosUsuarios.setBounds(298, 11, 519, 102);
			add(lblDatosUsuarios);
			JLabel lblinstruccion = new JLabel("");
			lblinstruccion.setText("<html><body><p style='text-align:left;font-family:verdana;font-size:13px;color:#7d7a86;'>Todas nuestras ofertas disponibles, si alguna te interesa s�lo tienes que clickear en ella y comprarla</p></body></html>");	
			lblinstruccion.setBounds(298, 119, 519, 45);
			lblDatosUsuarios.setBounds(298, 11, 519, 102);
			add(lblinstruccion);
		
			BotonOk btnAtras = new BotonOk("Volver",this.ventanaMadre);
			btnAtras.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					ventanaMadre.cargaPantallaMenuUsuario();
				}
			});
			btnAtras.setBounds(172, 601, 225, 35);;
			add(btnAtras);
			
			 BotonOk btnAceptar = new BotonOk("Filtrar",this.ventanaMadre);
			 btnAceptar.addMouseListener(new MouseAdapter() {
			 	@Override
			 	public void mouseClicked(MouseEvent e) {
			 		 //System.out.println("clickeo");
			 		ventanaMadre.cargaPantallaFiltrarOfertas();
			 	
			 	} });
			 
			  btnAceptar.setBounds(517, 601,225, 35);
			  add(btnAceptar);
			 	}
		
		
		
/**Para acceder al label que tiene la binevenida al usuairo
 * 		
 * @return 
 */

public JLabel getLblDatosUsuarios() {
	return lblDatosUsuarios;
}
/**Para acceder al label que tiene la binevenida al usuairo
 * para cambiar el texto.
 * @param lblDatosUsuarios
 */
public void setLblDatosUsuarios(JLabel lblDatosUsuarios) {
	this.lblDatosUsuarios = lblDatosUsuarios;
}
/**Cuando se compra una oferta autom�ticamente genera un fichero html
 * dd se4 imprimir�n todas las ofertas adquiridas en el mismo a�o
 */

public static void crearArchivo(int indice,FitpointJF ventanaMadre) {
	 try {
         // La clase file representa un archivo para crear,modificar o borrar
         //El constructor recive un String con la ruta absoluta o relativa del archivo
         //https://docs.oracle.com/javase/10/docs/api/java/io/File.html
         //String cadena="./fitp"+indice+"-"+LocalDate.now()+".html";
		 String cadena="./fitpoint-"+LocalDate.now().getYear()+".html";
         File archivo=new File(cadena);
		 /****/
         String cadena1="<html><body><table  align='center' width='70%'><tr><td  style='text-align:left;width:50%;margin:20px;padding:20px;'><img src='src/img/fitpoints.png'></td>" 
         		       +"<td style='text-align:right;;margin:20px;padding:20px;with:50%;font-family:verdana;font-size:20px;'><b>"+ventanaMadre.getUsuarioDAO().getNombre()+"<br>"+ventanaMadre.getUsuarioDAO().getApellidos()+"<br>"+ventanaMadre.getUsuarioDAO().getEmail()+"<br> DNI:"+ventanaMadre.getUsuarioDAO().getDni()+"</b></td></tr>"
         		       +"<tr style='height:65px;'><td colspan='2'></td></tr>"
         		       +"<tr style='height:50px;'><td colspan='2' style='padding:8px;background-color:#f9e8b8;'><p style='text-align:center;font-size:25px;font-family:verdana;'><b>Extracto de facturas anual</b></td></tr></table>"
        		       + "<table  align='center' width=70%' style='margin-top:50px;'>";
                        
         double precioEuro=0; 		        	 
 		
 		try {
 			//Es un imprimir oferta con una estructura mu parecida a impriir mis ofertas pero no igual (HTML)
 		     String aux=
 			cadena1=cadena1+ventanaMadre.getUsuarioDAO().imprimirMisOfertasAno(LocalDate.now().getYear())[0];
 		    String fitpointGastados=ventanaMadre.getUsuarioDAO().imprimirMisOfertasAno(LocalDate.now().getYear())[1];
 		   precioEuro=Integer.parseInt(fitpointGastados)*1.5;
 			
 		} catch (MisExcepciones e) {
 			JOptionPane.showMessageDialog(ventanaMadre, e.toString().substring(28),"Error",JOptionPane.ERROR_MESSAGE);
 		} catch (SQLException e1) {
 			JOptionPane.showMessageDialog(ventanaMadre, "Intentelo mas tarde..","Error",JOptionPane.ERROR_MESSAGE);
 		}
 		
 		cadena1+= "<tr style='height:50px;'><td colspan='3'></td></tr>"
 				   + "<tr><td colspan='2' style='padding:15px';><p style='text-align:right;font-size:20px;font-family:verdana;'><b>Total factura </b></p>"+
                    "</td><td style='padding:8px;background-color:#f9e8b8;'><p style='text-align:center;font-size:20px;font-family:verdana;'>"+precioEuro+ " euros"+
                     "</p></td></tr>";
 		cadena1=cadena1+"</table></body></html>";
		   
		/***  */
         //abrir archivo
         archivo.createNewFile();
               
         FileWriter writer = new FileWriter(archivo,false);
         //si ponemos el segundo parametro a true no machaca, sino que lo escribe detras
         //representa a objetos que pueden escribir en archivos.
         //Se construyen a partit de objetos File
        /* String plantilla="<html><head></head><body style='font-family:verdana;font-size:20px;'><table  align='center' width='80%'><tr><td colspan='2' style='text-align:left;width:50%;margin:20px;padding:20px;'><img src='src/img/fitpoints.png'></td></tr>"
         	    +"<tr><td style='text-align:left;margin:20px;padding:20px;'><b>Nombre Gimnasio<br>Direccion<br>CIF<br>Email</b></td>"
         		+"<td style='text-align:right;background-color:#f9e8b8;margin:20px;padding:20px;'><b>Nombre<br>Apellido1<br>DNI<br>direccion</b></td></tr></table>"
         	    +"<table  align='center' width='55%' style='border:3px dotted #FADE78;border-collapse:collapse;margin-top:100px;'>"
         		+"<tr><td colspan='3' style='text-align:center;color:#7d7a86;margin:30px;padding:30px;'><h2>Actividad</h2></td></tr>"   
         		+"<tr ><td style='text-align:center;color#7d7a86;margin:30px;padding:30px;width:33%;font-size:20px;'>MNumero de fitpoints</td><td style='text-align:center;color#7d7a86;margin:30px;padding:30px;width:33%;font-size:20px;'>Hora</td><td style='text-align:center;color#7d7a86;margin:30px;padding:30px;width:33%;font-size:20px;'>D�a</td></tr></table>" 
         		+ "</body></html>";*/
         
         writer.write(cadena1);
        
         //Escribe en el archivo la cadena que le indico
         //hasta que no haga flush, en el archivo no esta pasando nada
         writer.flush();
	 } catch (IOException ex) {
        ex.printStackTrace();
     }
       
}
	

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitpointsDAO;

import excepciones.MisExcepciones;
import fitpoint.Actividad;
import fitpoint.Gimnasio;
import fitpoint.Oferta;
import fitpointsDAO.Dao;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carmen
 */
final public class OfertaDAO extends Dao{
      private Oferta oferta;
   //Constructor//
     
    
      
      public OfertaDAO(Actividad id_actividad, Gimnasio id_gimnasio, Time hora, int id_oferta, short plazas, Date fecha,short num_fitpoints){ 
          //!!OJO Aqui falta implementar meterlo en la BBDD..lo hago luego
          //cuando de de alta los gimnasios
          super();     
          Oferta mioferta=new Oferta(id_actividad,id_gimnasio,hora,id_oferta,plazas,fecha,num_fitpoints);
           this.oferta=mioferta;  
      }   
       /**constructor que pasandole un id de oferta te devuelve el resto de valores que corresponde a esa oferta
      * A su vez te cre un objeto de la clase oferta
      * y para ello te tiene que crear tb un objeto de la clase gimnasio y otro de la clase actividad
      * 
      * @param id
      * @throws SQLException 
      */
    public OfertaDAO(int id) throws MisExcepciones {
          super();
          PreparedStatement smt=null;
          ResultSet rs=null;
          ActividadDAO miactividad=null;
          GimnasioDAO migimnasio=null;
          try {
            
              smt = getConn().prepareStatement("select * from fitpoints3.oferta where id_oferta=? order by fecha");
              smt.setInt(1,id);
              rs=smt.executeQuery();
              if (rs.next()){ //Si existe una oferta con ese ID
                  int id_actividad = rs.getInt("Actividades_id_actividad");
                  int id_gimnasio = rs.getInt("Gimnasio_id_gimansio");
                  Time hora = rs.getTime("hora");
                  Date fecha= rs.getDate("fecha");
                  short plazas = rs.getShort("plazas");
                  short num_fitpoints=rs.getShort("num_fitpoints");
                  /*A partir de aki hay que crear un objeto de la clase gimnasioDAO pasandole el id_gimnasio
                  y otro objeto de la clase actividad pasandole el id_actividad*/
                  miactividad=new ActividadDAO(id_actividad);
                  migimnasio=new GimnasioDAO(id_gimnasio);
                  //System.out.println("La hora de la oferta es:"+hora);
                  Oferta mioferta=new Oferta(miactividad.getActividad(),migimnasio.getGimnasio(),hora,id,plazas,fecha,num_fitpoints);
                  this.oferta=mioferta;
               }else{
                  throw new MisExcepciones("El id de la oferta no corresponse a ninguna oferta"); 
               }
          } catch (SQLException ex) {
              throw new MisExcepciones(ex.getMessage());
          } catch (MisExcepciones ex) {
              throw new MisExcepciones(ex.getMessage());
          }finally{
               try { 
                  if (rs!=null){ 
                     rs.close();
                  }
                   if (smt!=null){ 
                      smt.close();
                  }
                 
                } catch (SQLException ex) {
                  throw new MisExcepciones("Problemas al cerra la BBDD");
                }
          }
       }
      
      
   ///Getters and Setters///////////////////////////////////   
      public Oferta getOferta() {
        return oferta;
    }

    public void setOferta(Oferta oferta) {
        this.oferta = oferta;
    }
      public short getNum_fitpoints() {
        return oferta.getNum_fitpoints();
    }

    public void setNum_fitpoints(short num_fitpoints) {
        this.oferta.setNum_fitpoints(num_fitpoints); 
    }
    

    public Actividad getId_actividad() {
        return oferta.getId_actividad();
    }

    public void setId_actividad(Actividad id_actividad) {
        this.oferta.setId_actividad(id_actividad);
    }

    public Gimnasio getId_gimnasio() {
        return oferta.getId_gimnasio();
    }

    public void setId_gimnasio(Gimnasio id_gimnasio) {
        this.oferta.setId_gimnasio(id_gimnasio);
    }

    public Time getHora() {
        return oferta.getHora();
    }

    public void setHora(Time hora) {
        this.oferta.setHora(hora);
    }

    public int getId_oferta() {
        return oferta.getId_oferta();
    }

    public void setId_oferta(int id_oferta) {
        this.oferta.setId_oferta(id_oferta);
    }

    public short getPlazas() {
        return oferta.getPlazas();
    }

    public void setPlazas(short plazas) {
        this.oferta.setPlazas(plazas);
    }

    public Date getFecha() {
        return oferta.getFecha();
    }

    public void setFecha(Date fecha) {
        this.oferta.setFecha(fecha);
    }
   ////Fin getters and setters////////////////////////////////////////////////// 
    /**Imprime todos los datos de la oferta
     * Es una tabla que ir�a en cada regl�n de un JList 
     * cada rengl�n ser�a una oferta que pich�ndola se puede seleccionar
    **/
    public String imprimirOferta(){
        String cadena="";
        String timeStamp = new SimpleDateFormat("HH:mm").format(this.getHora());
        cadena="<html><body><table width='580px' style='margin-bottom:2px;'><tr style='background-color:#f2f2f2;border-left:3px dotted #fbe391'><td style='width:250px;'><p style='font-size:16px;font-family:verdana;'><b>"+this.getId_actividad().getNombre()+
                "</b></p></td><td style='width:165px;'><p style='font-size:16px;font-family:verdana;'>"+this.getId_gimnasio().getNombre()+
                "</p></td><td style='width:165px;'><p style='font-size:16px;font-family:verdana;'>"+this.getNum_fitpoints()+" fitpoints."+
                "</p></td></tr><tr style='background-color:#f2f2f2;border-left:3px dotted #fbe391'><td><p style='font-size:16px;font-family:verdana;'>"+this.getId_gimnasio().getDireccion()+
                "</p></td><td><p style='font-size:16px;font-family:verdana;'>"+timeStamp+
                "</p></td><td><p style='font-size:16px;font-family:verdana;'>"+this.getFecha()+               
                "</p></td></tr>"+"</table></body></html>";
                //"<tr style='height:1px;'><td colspan='3'></td></tr>"+
        return cadena;
    } 
    /**Devuelve una cadena <tr><td> ..los campos que quiero mostrar de las odetas q cre�
     * 
     * @return
     */
    public String imprimirMiOferta(){
    	 String timeStamp = new SimpleDateFormat("HH:mm").format(this.getHora());
       
        String  cadena="<tr style='background-color:#f2f2f2;'><td><p style='text-align:center;font-size:16px;font-family:verdana;'><b>"+this.getId_actividad().getNombre()+
                "</b></p></td><td><p style='text-align:center;font-size:16px;font-family:verdana;'>"+this.getId_gimnasio().getNombre()+
                "</p></td><td><p style='text-align:center;font-size:16px;font-family:verdana;'>"+this.getNum_fitpoints()+" fitpoints."+
                "</p></td></tr><tr style='background-color:#f2f2f2'><td><p style='text-align:center;font-size:16px;font-family:verdana;'>"+this.getId_gimnasio().getDireccion()+
                "</p></td><td><p style='text-align:center;font-size:16px;font-family:verdana;'>"+timeStamp+
                "</p></td><td><p style='text-align:center;font-size:16px;font-family:verdana;'>"+this.getFecha()+               
                "</p></td></tr>"
                 +"<tr style='height:1px;'><td colspan='3'></td></tr>";
        return cadena;
    } 
    /**funci�n auxiliar para cuando quiero crear el archivo html con las facturas anuales
     * Devuelve una cadena <tr><td> ..los campos que quiero mostrar de las odetas q cre�
     * @return
     */
    public String imprimirMiOfertaAno(){
   	 String timeStamp = new SimpleDateFormat("HH:mm").format(this.getHora());
      
       String  cadena="<tr style='background-color:#f2f2f2'><td style='padding:15px;'><p style='text-align:center;font-size:20px;font-family:verdana;'><i>"+this.getId_gimnasio().getNombre()+
               "</i></p></td><td style='padding:15px;'><p style='text-align:center;font-size:20px;font-family:verdana;'>"+this.getId_gimnasio().getDireccion()+
               "</p></td><td style='padding:15px;'><p style='text-align:center;font-size:20px;font-family:verdana;'> CIF:" +this.getId_gimnasio().getCIF()+             
               "</p></td></tr>"+
               "<tr style='background-color:#f2f2f2;'><td style='padding:15px;'><p style='text-align:center;font-size:20px;font-family:verdana;'><i>"+this.getId_actividad().getNombre()+
               "</i></p></td><td style='padding:15px;'><p style='text-align:center;font-size:20px;font-family:verdana;'>Hora: "+timeStamp+"    Fecha:"+this.getFecha()+ 
               "</p></td><td style='padding:15px;'><p style='text-align:center;font-size:20px;font-family:verdana;'>"+this.getNum_fitpoints()+" fitpoints / "+this.getNum_fitpoints()*1.5+" �"+
               "</p></td></tr>"+
               "<tr style='height:16px;'><td colspan='3'></td></tr>";
       return cadena;
   } 
   
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitpointsDAO;

import fitpoint.Usuario;
import validaciones.Validacion;
import constantes.Globals;
import excepciones.MisExcepciones;
import fitpoint.Oferta;
import static java.awt.event.PaintEvent.UPDATE;

import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JEditorPane;
import javax.swing.JOptionPane;

//import static jdk.nashorn.internal.runtime.JSType.isNumber;
import static validaciones.Validacion.validarApellidos;
import static validaciones.Validacion.validarDNI;
import static validaciones.Validacion.validarEmail;
import static validaciones.Validacion.validarFecha;
import static validaciones.Validacion.validarFitpoints;
import static validaciones.Validacion.validarMovil;
import static validaciones.Validacion.validarNombre;
import static validaciones.Validacion.validarNum_tarjeta;
import static validaciones.Validacion.validarPassword;
import static validaciones.Validacion.validarTelefono;

/**
 *
 * @author Carmen String todasOfertas=cd.verTodasLasOfertas();
 */
final public class UsuarioDAO extends Dao{

  
    private Usuario usuario;
   
   
     /******  gettters and setters*******************************************************************/    
      /***Implementamos todos los métodos que tiene usuario**/    
         public ArrayList<fitpoint.Oferta> getMisOfertas()  {
            return usuario.getMisofertas(); 
         } 
         public void setMisOfertas(ArrayList<fitpoint.Oferta> misofertas){
             usuario.setMisofertas(misofertas);
         }
       
   
         public Usuario getUsuario() {
                 return usuario;
           }
   

        public void setUsuario(Usuario usuario) {
                 this.usuario = usuario;
             }
      
            public String getDni() {
                return usuario.getDni();
            }

            public void setDni(String dni) {
               this.usuario.setDni(dni);
            }

            public String getNombre() {
                return usuario.getNombre();
            }

            public void setNombre(String nombre) {
                this.usuario.setNombre(nombre);
            }

            public String getApellidos() {
                return usuario.getApellidos();
            }

            public void setApellidos(String apellido1) {
                this.usuario.setApellidos(apellido1);
            }
           
            public int getTelefono() {
                return usuario.getTelefono();
            }

            public void setTelefono(String telefono) {
                this.usuario.setTelefono(telefono);
            }

            public int getMovil() {
                return usuario.getMovil();
            }

            public void setMovil(String movil) {
                this.usuario.setMovil(movil);
            }

            public String getEmail() {
                return usuario.getEmail();
            }

            public void setEmail(String email) {
                this.usuario.setEmail(email);
            }

            public String getNumerotarjeta() {
                return usuario.getNumerotarjeta();
            }

            public void setNumerotarjeta(String numerotarjeta) {
                this.usuario.setNumerotarjeta(numerotarjeta);
            }

            public LocalDate getFecha_cad() {
                return usuario.getFecha_cad();
            }

            public void setFecha_cad(String fecha_cad) {
                this.usuario.setFecha_cad(fecha_cad);
            }

            public String getPassword() {
                return usuario.getPassword();
            }

            public void setPassword(String password) {
                this.usuario.setPassword(password);
            }

            public short getFit_points() {
                return usuario.getFit_points();
            }

            public void setFit_points(short fit_points) {
                this.usuario.setFit_points(fit_points);
            }
       
   
   
    /**Constructor de usuario con todos los campos:Sirve para darse de alta principalamente
     * y tambien como auxiliar para otra funciones para creae un objeto usuario
     * Por parámetros se introducen todos los datos que necesita la Base de Datos
     * Crea un UsuarioDAo que sería un insertar directamente
     * @param dni
     * @param nombre
     * @param apellidos
     * @param telefono
     * @param movil
     * @param email
     * @param numerotarjeta
     * @param fecha_cad
     * @param password
     * @param fit_points
     * @param misofertas
     * @throws MisExcepciones
     * @throws SQLException 
     */     
    public UsuarioDAO(String dni, String nombre, String apellidos,String telefono,String movil, String email, String numerotarjeta,String fecha_cad,String password,Short fit_points,ArrayList<fitpoint.Oferta>misofertas) throws MisExcepciones{
        super();
        ResultSet resultado=null;
        ResultSet resultado1=null;
        PreparedStatement smt=null;
        String error="";
        // ArrayList<Oferta> ofertasCompradas=null;
       /*Se van a validar uno x uno todos los campos para que no sobrepasen la longitud del campo de la BBDD
        correspondiente,no se introduzcan vacíos donde en la BBDD son not null 
        en los campo que sean integer que la cedena de texto sea num�rica etc..
        */
        /*Si existen errores se van a concatenar en la variable error:
           -Por lo tanto si error="" -->Todo correcto
                         si error!=""-->Existe un error ..lanzo una excepcion con mensaje el error 
       */
       try{
          error=validarDNI(dni);
          error=error+validarEmail(email);
          error=error+validarNombre(nombre);
          error=error+validarApellidos(apellidos);
          error=error+validarPassword(password);
          error=error+validarNum_tarjeta(numerotarjeta);
          error=error+validarMovil(movil);
          error=error+validarTelefono(telefono);
          error=error+validarFecha(fecha_cad);
          error=error+validarFitpoints(fit_points);//No hace falta poruqe sería 0
       
                if (error.equals("")){//si no hay errores

                smt = getConn().prepareStatement("select * from fitpoints3.usuario where dni=?");
                smt.setString(1,dni);
                resultado=smt.executeQuery();
                     if(resultado.next()){//El dNI es no es válido*
                        throw new MisExcepciones("El dni no es válido está repetido");
                    } else{//Si es válido se mira que el email no está tampoco repetido
                          smt = getConn().prepareStatement("select * from fitpoints3.usuario where email=?");
                          smt.setString(1,email);
                          resultado1=smt.executeQuery();
                           if(resultado1.next()){//El email es no es válido*
                                throw new MisExcepciones("El email no es válido está repetido");
                           }else{//Si son válidos los dos..entonces introduzco al usuario
                                //Compruebo si tiene ofertas ya compradas:
                                               ArrayList<Oferta> ofertasCompradas= this.comprobarOfertas(dni) ;
                                               //Creo el usuarioy ofertasCompradas será [] si no tiene ofertas y un arrayList si si tiene
                                              this.usuario = new Usuario(dni,nombre,apellidos,telefono,movil, email, numerotarjeta, fecha_cad,password,fit_points,ofertasCompradas);
                                              //Lo meto en la BBDD: tlf,movil y lo cojo de usuario q tiene los tipos corectos
                                               smt.executeUpdate("INSERT INTO fitpoints3.Usuario(dni,nombre,apellidos,telefono,movil, email, num_tarjeta, fecha_caducidad,password,fit_points) VALUES('"+dni+"','"+nombre+"','"
                                               +apellidos+"','"+this.usuario.getTelefono()+"','"+this.usuario.getMovil()+"','"+email+"','"+numerotarjeta+"','"+this.usuario.getFecha_cad()+"','"+password+"','"+fit_points+ "')");
                                             //Introduzceo en un objeto Usuario
                                               }

                    } 
                  }else{//cdo hubo alguna validación erronea
                       
                	  throw new MisExcepciones(error);
                }   
         }catch (SQLException ex){
                  throw new MisExcepciones(ex.getMessage());
                
         }finally{
                  //cierro todo              
                        try { 
                            if (resultado1!=null){ 
                            resultado1.close();
                            }
                            if (resultado!=null){ 
                            resultado.close();
                            }
                             if (smt!=null){ 
                              smt.close();
                             }
                       } catch (SQLException ex) {
                             throw new MisExcepciones("Problemas al cerra la BBDD");
                        }
                 }
        
        }
   /**función a la que se le pasa el DNI ,busca en la tabla compras los id_oferta que compro
    * el usuario con ese dni.Saca todos los registros y lo mete en un ArrayList de ofertas
    * Y devuelve ese ArrayList de Oferta
    * @param dni
    * @return
    * @throws SQLException
    * @throws MisExcepciones 
    */     
    
  public ArrayList<Oferta> comprobarOfertas(String dni) throws MisExcepciones{
       //Compruebo si tiene ofertas ya compradas:
        PreparedStatement smt=null;
        ResultSet resultado1=null;
        ArrayList<Oferta> ofertasCompradas=new ArrayList<Oferta>();
            try {
               	smt = getConn().prepareStatement("select ofertas_id_oferta from fitpoints3.compras c where Usuario_dni=? ");
            	smt.setString(1,dni);
                 resultado1=smt.executeQuery();
                while (resultado1.next()){
                     int ofertaId=resultado1.getInt("ofertas_id_oferta");
                     OfertaDAO usuario_ofertas=new OfertaDAO(ofertaId);
                     ofertasCompradas.add(usuario_ofertas.getOferta());
                 }
               
                
            } catch (SQLException ex) {
                throw new MisExcepciones(ex.getMessage());
            } catch (MisExcepciones ex) {
                throw new MisExcepciones(ex.getMessage());
            }finally{
                 
                 //cierro todo
                 try {
                     if (resultado1!=null){
                         resultado1.close();
                     }
                     if (smt!=null){
                         smt.close();
                     }
                 } catch (SQLException ex) {
                     throw new MisExcepciones("Problemas al cerra la BBDD");
                 }  finally{
                      return ofertasCompradas;
                 } 
              }
              
    }
           
 
                   
         /**Para logearse..se le pasa el correo y el email
          * Si existe devuelve un resultset con todos los datos
          * Convierte ese registro de una BBDD en un Usuario
           * Cada campo lo mete en una variable correspondientes..y con estas variables
          * se crea un usuario.
          */          
        public UsuarioDAO(String correo,String password1) throws MisExcepciones, SQLException  {
        super();
         PreparedStatement smt=null;
         ResultSet rs=null;
         String error=validarEmail(correo)+validarPassword(password1);
           
        try {
           
            //Compruebo que los parámetros de entrada son correctos
            if (error.equals("")){//si lo son.
            
                smt = getConn().prepareStatement("select * from fitpoints3.usuario where email=? and password=?");
                smt.setString(1,correo);
                smt.setString(2,password1);
                rs=smt.executeQuery();
            
                if(rs.next()){//El email y password existen en la BBDD
                        Usuario usuario = null;//Devuelvo al usuario
                        //Leo todos los campos de la tabla usuario
                        String dni = rs.getString("dni");
                        String nombre = rs.getString("nombre");
                        String apellidos = rs.getString("apellidos");
                        int telefono = rs.getInt("telefono");
                        int movil = rs.getInt("movil");
                        String email = rs.getString("email");
                        String numerotarjeta = rs.getString("num_tarjeta");
                        Date fecha_cad = rs.getDate("fecha_caducidad");
                        /*DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
                        String fechaCadena = df.format(fecha_cad);*/
                        String password=rs.getString("password");
                        Short fit_points=rs.getShort("fit_points");
                        
                      //Compruebo si tiene ofertas ya compradas:
                       ArrayList<Oferta> ofertasCompradas= this.comprobarOfertas(dni) ;
                         
                        //Creo un usuario clase Usuario con esos datos y misofertas [] si no tiene
                          usuario = new Usuario(dni,nombre,apellidos,Integer.toString(telefono),Integer.toString(movil), email, numerotarjeta,fecha_cad.toString(),password,fit_points,ofertasCompradas);
                         
                          this.usuario=usuario;
                      

                } else{//Cuando no existen en la BBDD
                     throw new MisExcepciones("El email y password no existen");
                    } 
            }else{//Cuando lo que se pasa por parámetro no es válido
                throw new MisExcepciones(error);
                } 
        } catch (SQLException ex) {//Algun problema con la conexión..algo SQL
             throw new MisExcepciones(ex.getMessage());
        }finally{
              //cierro todo              
                       try { 
                            if (rs!=null){ 
                            rs.close();
                            }
                           
                             if (smt!=null){ 
                              smt.close();
                             }
                       } catch (SQLException ex) {
                             throw new MisExcepciones("Problemas al cerra la BBDD");
                        }
                  
        }
        }
       /**Imprime todos los datos del usuario menos las ofertas compradas que se hará en una funciçon aparte
        * 
        * @return Un Strin con todos los datos
        */
        public String imprimirUsuario(){
       
            String cadena="";
         cadena="<html><body><p style='font-size:12px;font-family:Verdana;font-color:#b7aa00'><b>"+this.getNombre()+ this.getApellidos()+"</b><br>"
                   +"Movil : "+this.getMovil()+"<br>"
                   +"Email: "+this.getEmail()+"<br> "+/*"Password "+this.getPassword()+"<br>"
                   +"Numero de tarjeta: "+this.getNumerotarjeta()+"<br>"+ "Fecha de caducidad de la tarjeta: "+this.getFecha_cad()+"<br>"
                   +*/"DNI: "+this.getDni()+"<br>"+"Numero de fitpoints actuales: "+this.getFit_points()+"<br></p></body></html>";
           return cadena;
        }
        /**imprime el nombre y apellido del usuario,los fitpoints que tiene y el numero de ofertas por gastar
         * Va en lblUsuario de todos los JPanel cuando ya est� registrado el usuario
         * @return devuelve una cadena con el Bienvenido
         */
        public String imprimirNombreUsuario(){
        	 int cont=0;
        	if (!(this.getMisOfertas().isEmpty())){//Si tiene asociadas ofertas
                 for (int i=0; i<this.getMisOfertas().size();i++){ ///Para que sólo saque las actuales
              	   if(this.getMisOfertas().get(i).getFecha().after(Date.valueOf(LocalDate.now()))){
                        cont++;
                     }
                 } 
        	}    
            
            String cadena="";
         cadena="<html><body><p style='font-size:18px;font-family:Verdana;color:#7d7a86;font-weight:normal'>Bienvenido :<i>"+this.getNombre()+"  "+ this.getApellidos()+"</i></p>"+
                 "<p style='font-size:14px;font-weight:normal;font-family:Verdana;color:#7d7a86'>Actualmente tienes  "+this.getFit_points()+" fitpoints en tu cuenta y "+cont+" ofertas compradas</p></body></html>";
           return cadena;
        }
        /** A esta función se le pasa una oferta y comprueba si ha sido comprada..
         * Es decir que sea un elemento en usuarios->misofertas
         * Devuelve true..si esta y false si no..
         */
        public boolean comprobarOferta(Oferta oferta){
            //System.out.println("Entro en comprobar oferta"+this.usuario.getMisofertas().size());
            boolean encontrado=false;
            int i=0;
            
            while ((!encontrado) && (i<this.usuario.getMisofertas().size())){
                                
                if (this.usuario.getMisofertas().get(i).getId_oferta()==oferta.getId_oferta()){
                   
                    encontrado=true;
                }else{
                 
                   i++; 
                }  
                
            }
            return encontrado;
        }
        
        
        
        /**A esta función se le pasa el id_oferta y realiza la compra de la oferta:Se le añade al array List de sus ofertas compradas,
         * le resta los fitpoints gastados,le resta el numero de plazas a la oferta,crea en compras un registro nuevo con dniusuario-idoferta
         * ..Todo esto sobre la  BBDD y el objeto usuaio directamente
         * */
        
        public void comprar(int id_oferta) throws MisExcepciones {
       
            PreparedStatement smt=null;
            ResultSet rs1=null;
            OfertaDAO oferta=null;
            try {
                    oferta=new OfertaDAO(id_oferta);
                    Oferta estaoferta=oferta.getOferta();
                    //Compruebo que no la haya comprado ya..
                    if (comprobarOferta(estaoferta)){//Si ya le ha comprado antes
                        throw new MisExcepciones("Esta oferta ya la tiene comprada elija otra");
                    }else{//No la ha comprado
                        //Lo primero de todo saber cuanto fitpoins vale y cuantos me keda..
                        //--En el objeto usuario
                        short fit=oferta.getNum_fitpoints();

                        if(this.usuario.getFit_points()>=fit){//Si tengo más fitpoints de lo que vale
                            //Se los resto al objeto Usuario
                            short total=(short) ((this.usuario.getFit_points())-(fit));
                            this.usuario.setFit_points(total);

                            //Lo modifico en la BBDD..le resto los fitpoints gastados
                            smt=getConn().prepareStatement("UPDATE fitpoints3.usuario SET fit_points=? where dni=?;");
                            smt.setShort(1,total);
                            smt.setString(2,this.usuario.getDni());
                            smt.executeUpdate();

                            //Para comprar una oferta-->Tengo que añadirselo al arraylist de ofertas del usuario
                            ArrayList<Oferta> mias= this.usuario.getMisofertas();
                            mias.add(estaoferta);
                            this.usuario.setMisofertas(mias);

                            //Tengo que añadir un registro a la tabla compras con :id_oferta y el dni del usuario
                            smt=getConn().prepareStatement("insert into fitpoints3.compras(usuario_dni,ofertas_id_oferta)values(?,?);");
                            smt.setString(1,this.getDni());
                            smt.setInt(2, id_oferta);
                            smt.executeUpdate();

                            //En la BBDD En la oferta habría que quitarle una plaza
                            //Lo que habia
                           
                            smt=getConn().prepareStatement("select plazas from fitpoints3.oferta where id_oferta=?");
                            smt.setInt(1,id_oferta);
                            rs1=smt.executeQuery();
                            if (rs1.next()){//Si hay concurrencia habria que mirar el numero de plaza>0
                                int num=rs1.getInt("plazas");
                                num--;
                                //Le resto y lo meto en la BBDD
                                smt=getConn().prepareStatement("UPDATE fitpoints3.oferta SET plazas=? where id_oferta=?;");
                                smt.setInt(1 ,num);
                                smt.setInt(2,id_oferta);
                                smt.executeUpdate();
                            }
                           

                        } else{
                            throw new MisExcepciones("No tienes los fitpoints necesarios para poder comprar la actividad .Recargelos por favor..");
                        }
                    }
                } catch (SQLException ex) {
                   throw new MisExcepciones(ex.getMessage());
                } catch (MisExcepciones ex) {
                   throw new MisExcepciones(ex.getMessage());
                }finally{
                     //cierro todo
                           try {
                                if (rs1!=null){
                                    rs1.close();
                                }
                                if (smt!=null){
                                    smt.close();
                                }
                              /*  if (oferta!=null){//Tb se cierra la conexión de oferta
                                    oferta.getConn().close();
                                }*/
                           }catch (SQLException ex) {
                                throw new MisExcepciones("Problemas al cerra la BBDD");
                           }
                 }
                } 

  
        /** Imprime todas las ofertas que el usuario ha comprado
                * y devuelve en un String encadenadado, cada una con todos su datos
                */

                public String imprimirMisOfertas() throws SQLException, MisExcepciones{
                   
                	String cadena="<table width='95%' align='center'>"; 
                    //System.out.println("-----Ofertas compradas--------");
                    ArrayList<Oferta>mias=new ArrayList<Oferta>();
                    mias=this.getMisOfertas(); 
                    OfertaDAO miofertaDAO=null;
                    boolean nocaducada=false;
                    if (!(mias.isEmpty())){//Si tiene asociadas ofertas
                       for (int i=0; i<mias.size();i++){ ///Para que sólo saque las actuales
                    	   if(mias.get(i).getFecha().after(Date.valueOf(LocalDate.now()))){
	                           miofertaDAO=new OfertaDAO(mias.get(i).getId_oferta());
	                           nocaducada=true;
	                           cadena=cadena+miofertaDAO.imprimirMiOferta();
                           }
                       } 
	                
	                   if(nocaducada==false){//El caso de q tenga ofertas pero toas están caducadas
	                      cadena="<p style='font-size:18px; color=#b7aa00'>No ha comprado ninguna oferta nueva..</p>";
	                   }  
                    }else{//Cuando no tiene ninguna oferta comprada
                        cadena="<p style='font-size:18px; color=#b7aa00'>Aun no ha comprado ninguna oferta</p>";
                    }
             cadena+="</table></body></html>";
           
             return cadena;
        }
         /***********************************************************************************************************/
                /** Imprime todas las ofertas que el usuario ha comprado este a�o
                 * y devuelve en un String encadenadado, cada una con todos su datos y el numero de fitpointsacumulados
                 * Se utiliza para sacar la factura anual
                 */
              
      public String[] imprimirMisOfertasAno(int ano) throws SQLException, MisExcepciones{
                    
                 	String cadena="";
                     //System.out.println("-----Ofertas compradas--------");
                     ArrayList<Oferta>mias=new ArrayList<Oferta>();
                     mias=this.getMisOfertas(); 
                     OfertaDAO miofertaDAO=null;
                     int fitpointsGastados=0;
                     boolean exite=false;
                     String formato="yyyy";
                     SimpleDateFormat dateFormat = new SimpleDateFormat(formato);
                     if (!(mias.isEmpty())){//Si tiene asociadas ofertas
                        for (int i=0; i<mias.size();i++){ ///Para que sólo saque las actuales
                        	Date date=mias.get(i).getFecha(); 
                        	int anoDate=Integer.parseInt(dateFormat.format(date)); 
                        	if(anoDate==(LocalDate.now().getYear())){ 
 	                           miofertaDAO=new OfertaDAO(mias.get(i).getId_oferta());
 	                           fitpointsGastados+=miofertaDAO.getOferta().getNum_fitpoints();
 	                           cadena=cadena+miofertaDAO.imprimirMiOfertaAno();
                            }
                        } 
 	                 	                  
                     }else{//Cuando no tiene ninguna oferta comprada
                         cadena="<tr><td><p style='font-size:18px; color=#b7aa00'>Aun no ha comprado ninguna oferta</p></td></tr>";
                     }
                     String respuesta[]= {cadena,fitpointsGastados+""};
                     
                     
                    		 
                          
              return respuesta;
         }
                
                
                
                
         
                
              
        /***Devuelve en una cadena todas las actividades
           * que están en fitpoints con todos sus datos
           * @return una cadena con todos los datos encadenados
           * @throws MisExcepciones 
           */
        
          public String verTodasActividades() throws MisExcepciones{
            PreparedStatement smt=null;
            ResultSet rs=null;
            String cadena="<table width='90%' align='center' style='text-align:center;font-size:16px;font-family:verdana;'>";
            ActividadDAO actividad=null;
            try {
            smt = getConn().prepareStatement("select * from fitpoints3.actividad a order by a.nombre ");
            rs=smt.executeQuery();
                if (rs.next()){
                    do{
                        int id=rs.getInt("id_actividad") ;
                        actividad=new ActividadDAO(id);
                       
                        cadena=cadena+actividad.imprimirActividad();
                    }while (rs.next());

                }else{
                    throw new MisExcepciones("No hay actividades dadas de alta actualmente") ;  
                }
               
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
                try {
                    if (rs!=null){
                       rs.close();
                    }
                    if (smt!=null){
                       smt.close();
                    }
                  /*  if (actividad!=null){//Tb se cierra la conexión de actividad
                       actividad.getConn().close();
                    }*/
                 }catch (SQLException ex) {
                    throw new MisExcepciones("Problemas al cerra la BBDD");
                 }finally{
                      cadena=cadena+"</table></body></html>";	 
                 
                      return cadena;
                }
         }  
       }
          /***Devuelve en una cadena todos los gimnasios 
           * que están en fitpoints con todos sus datos
           * @return una cadena con todos los datos encadenados
           * @throws MisExcepciones 
           */
      public  String verTodosLosGimnasios() throws MisExcepciones{
            PreparedStatement smt=null;
            ResultSet rs=null;
            String cadena="<table width='90%' align='center'>";
            try {
            smt = getConn().prepareStatement("select * from fitpoints3.gimnasio g order by nombre");
            rs=smt.executeQuery();
            int i=0;
                if (rs.next()){
                    do{
                    	
                        int id=rs.getInt("id_gimansio") ;
                     
                        GimnasioDAO gimnasio=new GimnasioDAO(id);
                       // cadena=cadena+gimnasio.imprimirGimnasio()+"\n"+"\n";
                      if (i%2==0) {
                    	  cadena=cadena+"<tr style='background-color:#eeeeee;border-left:3px dotted #fbe391;' >" ;
                      }else {
                    	  cadena=cadena+"<tr style=''>"; 
                      }
                      
                      cadena=cadena+"<td><p style='text-align:center;font-size:16px;font-family:verdana;'><b>"+gimnasio.getNombre()+"</b></p></td><td><p style='text-align:center;font-size:16px;font-family:verdana;'>"+gimnasio.getDireccion()+"</p></td><td><p style='text-align:center;font-size:16px;font-family:verdana;'>"+gimnasio.getTelefono()+"</p></td></tr>";
                      i++;
                    }while (rs.next());

                }else{
                    throw new MisExcepciones("No hay Gimnasios dados de alta actualmente") ;  
                }
                if (rs!=null){
                 rs.close();
               }
               if (smt!=null){
                  rs.close();
               }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try { 
                if (rs!=null){ 
                   rs.close();
                }
                 if (smt!=null){ 
                    smt.close();
                }
                /* if (miactividad!=null){
                     miactividad.getConn().close();
                 }
                 if (migimnasio!=null){
                     migimnasio.getConn().close();
                 }*/
              } catch (SQLException ex) {
                throw new MisExcepciones("Problemas al cerra la BBDD");
              }finally {
            	  cadena=cadena+"</table>";
                  return cadena;
              }
        }
          
       }
     
		/***Devuelve en un arrayList con todas las ofertas actuales
            * las que están pasadas de fecha no cuentan
           * que están en fitpoints con todos sus datos
           * @return el arrayList
           * @throws MisExcepciones 
           */
        
       public ArrayList<OfertaDAO> devolverTodasLasOfertas() throws MisExcepciones {
      
            PreparedStatement smt=null;
            ResultSet rs=null;
            String cadena="";
            OfertaDAO oferta=null;
            ArrayList<OfertaDAO> arrayOferta= new ArrayList <OfertaDAO>();
            try { 
            smt = getConn().prepareStatement("select * from fitpoints3.oferta o order by o.fecha");
            rs=smt.executeQuery();
            if (rs.next()){
                do{
                	 int numPlazas=rs.getInt("plazas");
                    int id=rs.getInt("id_oferta") ;
                    Date fecha=rs.getDate("fecha");
                    if (fecha.after(Date.valueOf(LocalDate.now()))&&(numPlazas>0)){
                        oferta=new OfertaDAO(id);
                        arrayOferta.add(oferta);
                        
                      
                    }
                }while (rs.next());
                
            }else{
                throw new MisExcepciones("No hay ofertas dados de alta actualmente") ;
            }
            
           
         }catch (MisExcepciones ex) {
             throw new MisExcepciones(ex.getMessage());
        
            
        } catch (SQLException ex) {
            throw new MisExcepciones(ex.getMessage());
        }finally{
              try {   
                    if (rs!=null){
                       rs.close();
                    }
                    if (smt!=null){
                    rs.close();
                    }
                  /*  if(oferta!=null){
                     oferta.getConn().close();
                    }*/
              }catch (SQLException ex) {
                    throw new MisExcepciones(ex.getMessage());
             }finally{
                  return arrayOferta;
            }
            
         }
        
       }
       /**funci�n que devuelve laas ofertas aplicandoles unos filtros
        * idgym-->es un entero que es el id_gimansio
        */
       public ArrayList<OfertaDAO> devolverOfertasfiltradas(int idgym,int idactividad,String sqlDate) throws MisExcepciones {
    	      
           PreparedStatement smt=null;
           ResultSet rs=null;
           String cadena="";
           OfertaDAO oferta=null;
           ArrayList<OfertaDAO> arrayOferta= new ArrayList <OfertaDAO>();
           try { 
        	 //  System.out.println("idGIMNASIO="+idgym+"idactividad"+idactividad+"fecha "+sqlDate);
           if (!sqlDate.equals("0")) {  //Si tiene una fecha concreta
	         if( idgym>0 &&idactividad>0)	{   
	        	 // System.out.println("Entro cuando estan pilladas todas las opciones");
		           smt = getConn().prepareStatement("select * from fitpoints3.oferta where Gimnasio_id_gimansio=? and Actividades_id_actividad=? and fecha=? ");
		           smt.setInt(1,idgym);
		           smt.setInt(2,idactividad);
		           smt.setString(3,sqlDate);
	           }
	           if( idgym>0 && idactividad==0){   
	               smt = getConn().prepareStatement("select * from fitpoints3.oferta where Gimnasio_id_gimansio=? and fecha=? ");
	               smt.setInt(1, idgym);
	               smt.setString(2,sqlDate);
	            }
	           if( idgym==0 && idactividad>0){   
	        	  // System.out.println("Deberia de entrar por id gimansio=0 y idactividad="+idactividad+" y la fecha es"+sqlDate);
	               smt = getConn().prepareStatement("select * from fitpoints3.oferta where Actividades_id_actividad=? and fecha=?");
	               smt.setInt(1,idactividad);
	               smt.setString(2,sqlDate);
	            }
	           if( idgym==0 && idactividad==0){   
	        	   //System.out.println("Deberia de entrar por id gimansio=0 y idactividad=0 y la fecha es"+sqlDate);
	               smt = getConn().prepareStatement("select * from fitpoints3.oferta where fecha=?");
	               smt.setString(1,sqlDate);
	            }
           }else {//Si es todas las fechas
        	 if( idgym>0 &&idactividad>0)	{   
 	        	//  System.out.println("Entro cuando estan pilladas todas las opciones");
 		           smt = getConn().prepareStatement("select * from fitpoints3.oferta where Gimnasio_id_gimansio=? and Actividades_id_actividad=? ");
 		           smt.setInt(1,idgym);
 		           smt.setInt(2,idactividad);
 		          
 	           }
 	           if( idgym>0 && idactividad==0){   
 	               smt = getConn().prepareStatement("select * from fitpoints3.oferta where Gimnasio_id_gimansio=?");
 	               smt.setInt(1, idgym);
 	            }
 	           if( idgym==0 && idactividad>0){   
 	        	//   System.out.println("Deberia de entrar por id gimansio=0 y idactividad="+idactividad+" y la fecha es"+sqlDate);
 	               smt = getConn().prepareStatement("select * from fitpoints3.oferta where Actividades_id_actividad=?");
 	               smt.setInt(1,idactividad);
 	               
 	            }
 	           if( idgym==0 && idactividad==0){   
 	        	   System.out.println("Deberia de entrar por id gimansio=0 y idactividad=0 y la fecha es"+sqlDate);
 	               smt = getConn().prepareStatement("select * from fitpoints3.oferta ");
 	               
 	            }
        	   
        	   
           }
           
           
           rs=smt.executeQuery();
           if (rs.next()){//Esto no har�a falta pq ya en el select cojo una fecha que desde fuera compruebo que sea mayor q hoy
               do{        //pero no est� de mal y tb comprueba que el numero de plazas sea mayor q 0
                   int id=rs.getInt("id_oferta") ;
                   Date fecha=rs.getDate("fecha");
              	    int numPlazas=rs.getInt("plazas");
                   if (fecha.after(Date.valueOf(LocalDate.now()))&&(numPlazas>0)){
                       oferta=new OfertaDAO(id);
                       arrayOferta.add(oferta);
                       
                     
                   }
               }while (rs.next());
               
           }else{
               throw new MisExcepciones("No hay ofertas dados de alta actualmente") ;
           }
           
          
        }catch (MisExcepciones ex) {
            throw new MisExcepciones(ex.getMessage());
       
           
       } catch (SQLException ex) {
           throw new MisExcepciones(ex.getMessage());
       }finally{
             try {   
                   if (rs!=null){
                      rs.close();
                   }
                   if (smt!=null){
                      smt.close();
                   }
                 
             }catch (SQLException ex) {
                   throw new MisExcepciones(ex.getMessage());
            }finally{
                return arrayOferta;
           }
           
        }
       
      }
          
   
    /**Constructor de usuario con todos los campos:Sirve para darse de alta principalamente
     * y tambien como auxiliar para otra funciones para creae un objeto usuario
     * Por parámetros se introducen todos los datos que necesita la Base de Datos
     * Crea un UsuarioDAo que sería un insertar directamente
     * @param dni
     * @param nombre
     * @param apellidos
     * @param telefono
     * @param movil
     * @param email
     * @param numerotarjeta
     * @param fecha_cad
     * @param password
     * @param fit_points
     * @param misofertas
     * @throws MisExcepciones
     * @throws SQLException 
     */     
    public void modificarUsuario(String nombre, String apellidos,String telefono,String movil, String email, String numerotarjeta, String fecha_cad,String password) throws MisExcepciones{
        ResultSet resultado=null;
        ResultSet resultado1=null;
        PreparedStatement smt=null;
        PreparedStatement smt1=null;
        String error="";
       /*Se van a validar uno x uno todos los campos para que no sobrepasen la longitud del campo de la BBDD
        correspondiente,no se introduzcan vacíos donde en la BBDD son not null etc..
        */
        /*Si existen errores se van a concatenar en la variable error:
           -Por lo tanto si error="" -->Todo correcto
                         si error!=""-->Existe un error ..lanzo una excepcion con mensaje el error 
       */
      
         error=error+validarEmail(email);
         error=error+validarNombre(nombre);
         error=error+validarApellidos(apellidos);
         error=error+validarPassword(password);
         error=error+validarNum_tarjeta(numerotarjeta);
         error=error+validarMovil(movil);
         error=error+validarTelefono(telefono);
         error=error+validarFecha(fecha_cad);
         try {
                if (error.equals("")){
                    //si no hay errores
                    smt = getConn().prepareStatement("select * from fitpoints3.usuario where email=?");
                    smt.setString(1,email);
                    resultado1=smt.executeQuery();
                    if(resultado1.next()){//El email es repetido*
                         String indice=resultado1.getString("dni");//compruebo si el DNI de ese email coincide con el m�o
                         if (!indice.equals(this.getDni())) {   //si es as� no hay problema..y si no ese email ya estar�a pillado
                    	   throw new MisExcepciones("El email no es valido ya existe");//Lanzo la excepci�n
                         }
                    }
                       //1� Cambio el usuario
                        this.setNombre(nombre);
                        this.setApellidos(apellidos);
                        this.setEmail(email);
                        this.setFecha_cad(fecha_cad);
                        this.setMovil(movil);
                        this.setNumerotarjeta(numerotarjeta);
                        this.setPassword(password);
                        this.setTelefono(telefono);
                       
                       //2� Lo cambio en la BBDD
                                         
                        smt1 = getConn().prepareStatement("UPDATE fitpoints3.usuario SET nombre=?,apellidos=?,telefono=?,movil=?,email=?,password=?,num_tarjeta=?,fecha_caducidad=?  where dni=?");
                        smt1.setString(1,nombre);
                        smt1.setString(2,apellidos);
                        smt1.setInt(3,Integer.parseInt(telefono));
                        smt1.setInt(4,Integer.parseInt(movil));
                        smt1.setString(5,email);
                        smt1.setString(6,password);
                        smt1.setString(7,numerotarjeta);
                        smt1.setDate(8,Date.valueOf(this.usuario.getFecha_cad())); 
                        smt1.setString(9,this.getDni());
                        smt1.executeUpdate();
                       
                        
        
                 }else{//cdo hubo alguna validación erronea
                     throw new MisExcepciones(error);
                } 
         }catch (SQLException ex) {
            throw new MisExcepciones(ex.getMessage());
         }finally{
              try { //cierro todo
                 if (resultado1!=null){
                     resultado1.close();
                 }
                 if (resultado!=null){
                     resultado.close();
                 }
                 if (smt!=null){
                     smt.close();
                 }
                 if (smt1!=null){
                     smt.close();
                 }
              } catch (SQLException ex) {
                throw new MisExcepciones("Problemas al cerra la BBDD");
               }
             
         }
        
   }
    /**funcion a la que se llama depues de darle al boton de confirmar la compra
     * 
     * @param num tipo entero s obtiene dl txtfield..una vez validado y pasado de String a int
     * luego hay que pasarlo a short para introducirlo en el objeto usuario
     * @throws MisExcepciones
     */
    public void compraFitpoint(int num) throws MisExcepciones {
   	    //Primero se lo sumo a los fitpoiints que ya ten�a el objeto 
    	short ftp=this.getFit_points();//Lo que tenia
	     this.setFit_points((short)(ftp+num));
	    //Lo cambio tb en la BBDD
	     PreparedStatement smt=null; 
	     try {
			smt = getConn().prepareStatement("UPDATE fitpoints3.usuario SET fit_points=? where dni=?");
			smt.setShort(1,getFit_points());
			smt.setString(2, getDni());
		    smt.executeUpdate();
		} catch (SQLException e) {
			 throw new MisExcepciones("Error,int�ntelo m�s tarde");
		}finally { 
			if (smt!=null){
	            try {
					smt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					 throw new MisExcepciones("Problemas al cerra la BBDD");
				}
             }
			
		}
   	      
   	 
   	 
    }
          
        
}

                       
          
            
 


   

  
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitpointsDAO;

import excepciones.MisExcepciones;
import fitpoint.Gimnasio;
import fitpointsDAO.Dao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Carmen
 */
final public class GimnasioDAO extends Dao{
    private Gimnasio gimnasio;
    
  
    public GimnasioDAO(String CIF, String nombre, String direccion, String gerente, int telefono, String email, String cc, String password) {
        super();
        //Tengo que implementarlo con todas las validaciones
    }
   
    
    /**Este constructor pasandole el id_gimnasio te devuelve todos los datos del gimnasio
     * en un objeto GimnasioDAO.No hay validación de id_gimnasio porque en ningun momento lo manipula el usuario
     * @param id_gimnasio :la clave primaria de la tabla Gimnasio
     */
    public GimnasioDAO(int id_gimnasio) throws SQLException, MisExcepciones{
        super();
        PreparedStatement smt=null;
        ResultSet rs=null;
       try {
        smt=getConn().prepareStatement("select * from fitpoints3.gimnasio where id_gimansio=?");
        smt.setInt(1,id_gimnasio);
        rs=smt.executeQuery();
       
	        if (rs.next()){//Si lo ha encontrado,existe en la BBDD
	           String nombre=rs.getString("nombre");
	           String direccion=rs.getString("direccion");
	           String cif=rs.getString("CIF");
	           String cc=rs.getString("cc");
	           String email=rs.getString("email");
	           int telefono=rs.getInt("telefono");
	           String gerente=rs.getString("gerente");
	           String password=rs.getString("password");
	           Gimnasio gimnasio=new Gimnasio(cif,nombre,direccion,gerente,telefono,email,cc,password,id_gimnasio); 
	           this.gimnasio=gimnasio;
	        }else{
	            throw new  MisExcepciones("El identificador no corresponde a ningun gimnasio");
	        }
       } catch (SQLException ex) {
           throw new MisExcepciones(ex.getMessage());
       }finally{
            try { 
               if (rs!=null){ 
                  rs.close();
               }
                if (smt!=null){ 
                   smt.close();
               }
              
             } catch (SQLException ex) {
               throw new MisExcepciones("Problemas al cerra la BBDD");
             }
       }
    }
   
    

       
     

    /********************GETTERS and SETTERS********************************************************/
    public Gimnasio getGimnasio(){
         return gimnasio;
          
      }
    public void setGimnasio(Gimnasio gimnasio) {
        this.gimnasio = gimnasio;
    }
     public String getCIF() {
        return gimnasio.getCIF();
    }

    public void setCIF(String CIF) {
        this.gimnasio.setCIF(CIF);
    }     

    public String getNombre() {
        return gimnasio.getNombre();
    }

    public void setNombre(String nombre) {
        this.gimnasio.setNombre(nombre);
    }

    public String getDireccion() {
        return gimnasio.getDireccion();
    }

    public void setDireccion(String direccion) {
        this.gimnasio.setDireccion(direccion);
    }

    public String getGerente() {
        return gimnasio.getGerente();
    }

    public void setGerente(String gerente) {
        this.gimnasio.setGerente(gerente);
    }

   

    public String getEmail() {
        return gimnasio.getEmail();
    }

    public void setEmail(String email) {
        this.gimnasio.setEmail(email);
    }

    public int getTelefono() {
        return gimnasio.getTelefono();
    }

    public void setTelefono(int telefono) {
        this.gimnasio.setTelefono(telefono);
    }

    public String getCc() {
        return gimnasio.getCc();
    }

    public void setCc(String cc) {
        this.gimnasio.setCc(cc);
    }
   
    public int getId_gimnasio() {
        return gimnasio.getId_gimnasio();
    }

    public void setId_gimnasio(int id_gimnasio) {
        this.gimnasio.setId_gimnasio(id_gimnasio);
    }

    public String getPassword() {
        return gimnasio.getPassword();
    }

    public void setPassword(String password) {
        this.gimnasio.setPassword(password);
    }


    /********************END GETTERS and SETTERS****************************************************/
    public String imprimirGimnasio() {
        String cadena="";
        cadena="Nombre: "+this.getNombre()+"\n"+"Dirección: "+this.getDireccion()+"\n"+
               "CIF: "+this.getCIF()+"\n"+"CC: "+this.getCc()+"\n"+"Email: "+this.getEmail()+"\n"+
                "Teléfono: "+ this.getTelefono()+"\n"+"Gerente"+this.getGerente()+"\n"+
                "Password: "+this.getPassword();
        return cadena;
    }
    
}

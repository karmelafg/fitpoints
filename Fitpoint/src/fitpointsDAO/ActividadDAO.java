/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitpointsDAO;

import fitpoint.Actividad;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import excepciones.MisExcepciones;
/**
 *
 * @author Carmen
 */
/**Le paso el id de la actividad y te devuelve todos los datos**/
final public class ActividadDAO extends Dao {
      private Actividad actividad;
      /**Crea un objeto ActividadDao pasandole solamente el id de la actividad
       * 
       * @param id
       * @throws SQLException
       * @throws MisExcepciones 
       */ 
      public ActividadDAO(int id) throws SQLException,MisExcepciones {
          super();
            PreparedStatement smt=null;
            ResultSet rs=null;
            smt = getConn().prepareStatement("select * from fitpoints3.actividad where id_actividad=?");
            smt.setInt(1,id);
            rs=smt.executeQuery();
            try {
	            if(rs.next()){
	                String nombre= rs.getString("nombre");
	                String descripcion= rs.getString("descripcion");
	                Actividad actividad=new Actividad(id,nombre,descripcion); 
	                this.actividad=actividad;
	            } else{
	                throw new MisExcepciones("La consulta sobre la actividad no tiene resultados") ;  
	            }
            }catch (SQLException ex) {
                throw new MisExcepciones(ex.getMessage());
            }finally{
                 try { 
                    if (rs!=null){ 
                       rs.close();
                    }
                     if (smt!=null){ 
                        smt.close();
                    }
                   
                  } catch (SQLException ex) {
                    throw new MisExcepciones("Problemas al cerra la BBDD");
                  }
            }
       }
      /**Constructor al que se le pasan todos los parámetros
       * 
       * @param id
       * @param nombre
       * @param descripcion 
       */
       public ActividadDAO(int id,String nombre,String descripcion){
          //1ºMeterlo en la BBDD,como en un principio lo va a hacer el administrador no lo voy a imlpementar por ahora
          //2º Crear el objeto actualidad 
           Actividad actividad=new Actividad(id,nombre,descripcion); 
            this.actividad=actividad;
       }
       /**Este método imprime todos los datos de la actividad
        * 
        * @return 
        */
        public String imprimirActividad(){
            String cadena="";
            cadena="<tr style='text-align:left;'><td><b>-<span style='font-size:21px;color:#fbe391'>[</span>"+this.getNombre()+"<span style='font-size:21px;color:#fbe391'>]</span>-</b></td></tr><tr><td><p  style='font-size:14px;color:#7d7a86'>"+ this.getDescripcion()+"</p></td></tr><tr><td colspan='3' height:1px background_color:white></td></tr>";
           System.out.println(cadena);
            return cadena;
        }  

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }
       
    public int getId_actividad() {
        return actividad.getId_actividad();
    }

    public void setId_actividad(int id_actividad) {
        this.actividad.setId_actividad(id_actividad);
    }

    public String getNombre() {
        return actividad.getNombre();
    }

    public void setNombre(String nombre) {
        this.actividad.setNombre(nombre);
    }

    public String getDescripcion() {
        return actividad.getDescripcion();
    }

    public void setDescripcion(String descripcion) {
        this.actividad.setDescripcion(descripcion);
    }
    
    
}
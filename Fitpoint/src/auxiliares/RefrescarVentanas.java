package auxiliares;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import excepciones.MisExcepciones;
import interfaces.FitpointJF;

final public class RefrescarVentanas {
 public static void refresh(FitpointJF l) {
	 if(l.getPantallaMenuUsuario()!=null) {
	  l.getPantallaMenuUsuario().getLblDatosUsuarios().setText(l.getUsuarioDAO().imprimirNombreUsuario());
	 } 
	 if( l.getPantallaTodosGimnasios()!=null) {
	  l.getPantallaTodosGimnasios().getLblDatosUsuarios().setText(l.getUsuarioDAO().imprimirNombreUsuario());
	 }
	 if( l.getPantallaTodasOfertas()!=null) {
	  l.getPantallaTodasOfertas().getLblDatosUsuarios().setText(l.getUsuarioDAO().imprimirNombreUsuario());
	 }
	 if( l.getPantallaTodasActividades()!=null) {
	  l.getPantallaTodasActividades().getLblDatosUsuarios().setText(l.getUsuarioDAO().imprimirNombreUsuario());
	 }
	 if( l.getPantallaModificarUsuario()!=null) {
	  l.getPantallaModificarUsuario().getLblDatosUsuarios().setText(l.getUsuarioDAO().imprimirNombreUsuario());
	 } 
	 if( l.getPantallaMisOfertas()!=null) {
	 	  l.getPantallaMisOfertas().getLblDatosUsuarios().setText(l.getUsuarioDAO().imprimirNombreUsuario());
	 	  l.getPantallaMisOfertas().getEditorPane().setVisible(false);
	 	   String cadena="<html><body>";
			try {
				
				String cadena1=l.getUsuarioDAO().imprimirMisOfertas();
				  cadena=cadena+cadena1;
			} catch (MisExcepciones e) {
				 JOptionPane.showMessageDialog(l, e.toString().substring(28),"Error",JOptionPane.ERROR_MESSAGE);
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(l, "Intentelo mas tarde..","Error",JOptionPane.ERROR_MESSAGE);
			}
					
			cadena=cadena+"</body></html>";
			//System.out.println(cadena);
			 l.getPantallaMisOfertas().getEditorPane().setText(cadena);
	 	  	 l.getPantallaMisOfertas().getEditorPane().setVisible(true);
	 	 
	 }
	 if( l.getPantallaMenuUsuario()!=null) {
	 	  l.getPantallaMenuUsuario().getLblDatosUsuarios().setText(l.getUsuarioDAO().imprimirNombreUsuario());
	 }
	 if( l.getPantallaFiltrarOfertas()!=null) {
	 	  l.getPantallaFiltrarOfertas().getLblDatosUsuarios().setText(l.getUsuarioDAO().imprimirNombreUsuario());
	 }
 }


}

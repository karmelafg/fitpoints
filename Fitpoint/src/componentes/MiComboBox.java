package componentes;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JComboBox;
import javax.swing.border.LineBorder;

import interfaces.FitpointJF;

final public class MiComboBox extends JComboBox {
	private FitpointJF ventanaMadre;
	public MiComboBox(FitpointJF l) {
		
		super();
		setBackground(SystemColor.control);
		this.ventanaMadre=l;
		this.setFont(new Font("Verdana", Font.PLAIN, 20));
		this.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
}
}

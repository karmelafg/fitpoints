package componentes;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;

import interfaces.FitpointJF;


final public class BotonOk extends JButton{
	private FitpointJF ventanaMadre;
	public BotonOk(String txt,FitpointJF l) {
		super(txt);	
		setForeground(new Color(0,0,0));
		this.ventanaMadre=l;
		
		//setBackground(SystemColor.control);
		setBackground(new Color(251,227,145));
		
		setBorder(new LineBorder(Color.BLACK, 1, true));
		setFont(new Font("Verdana", Font.PLAIN, 20));
		
	    this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				setBorder(new LineBorder(Color.getColor("Micolor",new Color(203, 158, 7)),2, true));
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				setForeground(new Color(203, 158, 7));
				
				
				//hazLogin();
			}
			@Override
			public void mouseEntered(MouseEvent e) {
					
				setBorder(new LineBorder(Color.getColor("Micolor",new Color(203, 158, 7)),2, true));
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				setForeground(new Color(203, 158, 7));
			
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				setBorder(new LineBorder(Color.BLACK, 1, true));
				setForeground(new Color(0,0,0));
				
			
			}
			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				
			}
		});
	   
	}
	
	
	

}

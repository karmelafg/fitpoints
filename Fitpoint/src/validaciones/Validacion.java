/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validaciones;

import java.sql.Date;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carmen
 */
final public class Validacion {
    /****************todas las validaciones necesarias********************************/
  /**comprueba que el numero de fitpoints sea positivo y además que no superre le rango máximo de short..aunque nesto último no haría falta
   * 
   * @param fitpoints
   * @return 
   */
    public static String validarFitpoints(String fitpoints){
    String error="";
   
	    try {
	       int cad=Integer.parseInt(fitpoints);
	       if ((cad>500 )||(cad<0)){
		         error="El numero de fitpoints debe ser entre 0 y 500"+"\n";
		     }
	    }catch (NumberFormatException excepcion) {
	         error="El campo de fitpoints  no es valido"+"\n";
	       
	    }
	    
        return error;  
 }
    public static String validarFitpoints(int fitpoints){
        String error="";
       
    	  
    	       if ((fitpoints>500 )||(fitpoints<0)){
    	    	   error="El numero de fitpoints debe ser entre 0 y 500"+"\n";
    		     }
    	       	    
            return error;  
     }
    
    /**
     * Función que sirve para validar un DNI ..que cumla el patrón del DNI
     * @param dni:se le introduce el dni que se quiere insertar
     * @return devuelve "" si todo es correcto y si no es así te especifica el error en un string
     */ 
    public static String validarDNI(String dni){
         String error="";
           if (dni.equals("")){
               error="El dni no puede estar vacio"+"\n";
           }else{
                Pattern pattern = Pattern.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
                Matcher matcher = pattern.matcher(dni);
                    if (matcher.matches()) {
                        String letra = matcher.group(2);
                        String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
                        int index = Integer.parseInt(matcher.group(1));
                        index = index % 23;
                        String reference = letras.substring(index, index + 1);
                            if (reference.equalsIgnoreCase(letra)) {
                                 //Esta bien   correcto = true;
                            } else {
                               error="El dni no es correcto"+"\n";
                            }

                      } else {
                               error="El dni no es correcto"+"\n";
                      }
           }
         return error;
     }  
    /**Función que pasandole un string que corespondería a un email
     * nos dice si esta relleno(pq no puede ser vacío) o si el numero de
     * caracteres es mayor que la longitud del varchar de la BBDD que corresponde
     * 
     * @param email
     * @return error que estaría en blanco si tod ha ido correcto y si no devuelve el error correspondiente
     */
 public static String validarEmail(String email){    
     String error="";
     if (email.equals("")){
         error="El email es un campo obligatorio"+"\n";
     }else{
          if(email.length()>20){
              error="El email tiene demasiados caracteres"+"\n";
          }
          if((email.indexOf('@')==-1)||(email.indexOf('.')==-1)) {
        	  error="El email tiene un formato incorrecto"+"\n";
        	  
          }
     }
     return error; 
  }
 
 public static String validarApellidos(String apellidos){
    String error="";
    
     if (apellidos.equals("")){
         error="Apellidos es un campo obligatorio"+"\n";
     }else{
          if(apellidos.length()>45){
              error="Apellidos  tiene demasiados caracteres"+"\n";
          }
     }
     return error;  
 }
 public static String validarNombreGim(String nombre){
    String error="";
    
     if (nombre.equals("")){
         error="El nombre del gimnasio es un campo obligatorio es un campo obligatorio"+"\n";
     }else{
          if(nombre.length()>45){
              error="El nombre del gimnasio es incorrecto,tiene demasiados caracteres"+"\n";
          }
     }
     return error;  
 }
 public static String validarDireccion(String direccion){
    String error="";
    
     if (direccion.equals("")){
         error="La direccion del gimnasio es un campo obligatorio es un campo obligatorio"+"\n";
     }else{
          if(direccion.length()>45){
              error="La direccion del gimnasio es incorrecta,tiene demasiados caracteres"+"\n";
          }
     }
     return error;  
 }
 public static String validarGerente(String gerente){
    String error="";
    
     if (gerente.equals("")){
         error="El nombre del gerente del gimnasio es un campo obligatorio es un campo obligatorio"+"\n";
     }else{
          if(gerente.length()>45){
              error="El nombre del gerente del gimnasio es incorrecto,tiene demasiados caracteres"+"\n";
          }
     }
     return error;  
 }
 public static String validarNombre(String nombre){
    String error="";
    
     if (nombre.equals("")){
         error="El nombre es un campo obligatorio"+"\n";
     }else{
          if(nombre.length()>20){
              error="El nombre  tiene demasiados caracteres"+"\n";
          }
     }
     return error;  
 }
 public static String validarPassword(String password){
    String error="";
    
     if (password.equals("")){
         error="La password es un campo obligatorio"+"\n";
     }else{
          if(password.length()>10){
              error="La password tiene demasiados caracteres"+"\n";
          }
     }
     return error;  
 }
  public static String validarCIF(String cif){
    String error="";
    
     if (cif.equals("")){
         error="El CIF es un campo obligatorio"+"\n";
     }else{
          if(cif.length()>10){
              error="El CIF no es correcto,tiene demasiados caracteres"+"\n";
          }
     }
     return error;  
 }
   public static String validarCc(String cc){
    String error="";
    
     if (cc.equals("")){
         error="El numero de cuenta corriente es un campo obligatorio"+"\n";
     }else{
          if(cc.length()>15){
              error="El numero de cuenta corriente no es correcto,tiene demasiados caracteres"+"\n";
          }else{
               try {
                long ccd=Long.parseLong(cc);
                
              } catch (NumberFormatException excepcion) {
                  error="El numero de cuenta corriente debe ser un número"+"\n";
            }
          }
     }
     return error;  
 }
 public static String validarNum_tarjeta(String num_tarjeta){
    String error="";
    
     if (num_tarjeta.equals("")){
         error="El nuero de tarjeta es un campo obligatorio"+"\n";
     }else{
          if(num_tarjeta.length()>16){
              error="El numero de tarjeta tiene demasiados caracteres"+"\n";
          }else{
              try {
                long cad=Long.parseLong(num_tarjeta);
                
              } catch (NumberFormatException excepcion) {
                  error="El numero de tarjeta debe ser un numero"+"\n";
            }
          }
     }
     return error;  
 }
public static String validarMovil(String movil1){
    String error="";
    if (movil1.equals("")){
        error="El movil es un campo obligatorio"+"\n";
    } else{    
        error=isNumeric(movil1,"movil");
        if (error.equals("")){
           int movil=Integer.parseInt(movil1);
            if ((movil>999999999)||(movil<600000000)){
                error="El numero de movil no es valido"+"\n";
            }
        }    
   }         
     
     return error;  
 }
public static  String validarTelefono(String telefono1){//Si est� vac�o luego en en el setUsuario lo pongo a =
     String error="";
      if (!telefono1.equals("")){//Si  no est� vac�
	      error=isNumeric(telefono1,"telefono");
	        if (error.equals("")){
	           int telefono=Integer.parseInt(telefono1);
			     if ((telefono>999999999)||(telefono<900000000)){
			         error="El numero de telefono no es valido"+"\n";
	   		     }
	   		}
	  } 			 
     
     return error;  
}
/** Valida que la fecha es posterior a hoy
 * @param fecha_cad la fecha que le pasamos
 * @return error="" si no hay error o un mensaje cuando se produzca un error
 */

    /**
     * Valida que la fecha es posterior a hoy
     * @param fecha_cad1
     * @param fecha_cad la fecha que le pasamos
     * @return error="" si no hay error o un mensaje cuando se produzca un error
     */
    public static String validarFecha(String fecha_cad1){
     String error="";
    if (fecha_cad1.equals("")){
         error="La fecha de caducidad es un campo obligatorio";
     }else{ 
        try {
    	 LocalDate fn=LocalDate.parse(fecha_cad1);
         if(fn.isBefore(LocalDate.now())) {
            error="La fecha de caducidad es erronea";
		 }
        }catch(java.time.format.DateTimeParseException e) {
        	error="El formato de la fecha no es valido yyyy-mm-dd";
        	
        }
	     
      }	  
     return error;  
  }
 
  public static String isNumeric(String cadena,String campo) {
      String error="";
      try {
         int cad=Integer.parseInt(cadena);
      }catch (NumberFormatException excepcion) {
           error="El campo "+campo+"  debe ser un numero"+"\n";
         
      }
	return error;
}


}

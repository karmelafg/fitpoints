/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitpoint;

/**
 *
 * @author 1DAM
 */
final public class Compras {
    private Usuario id_usuario;
    private Oferta id_oferta;

    public Compras() {
    }

    public Compras(Usuario id_usuario, Oferta id_oferta) {
        this.id_usuario = id_usuario;
        this.id_oferta = id_oferta;
    }

    public Usuario getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Usuario id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Oferta getId_oferta() {
        return id_oferta;
    }

    public void setId_oferta(Oferta id_oferta) {
        this.id_oferta = id_oferta;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitpoint;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author 1DAM
 */
final public class Usuario {
    private String  dni;
    private String nombre;
    private String apellidos;
    private int telefono;
    private int movil;
    private String email;
    private String numerotarjeta;
    private LocalDate fecha_cad;
    private String password;
    private short fit_points;
    private ArrayList<Oferta> misofertas=new ArrayList<Oferta>();    
   

    public Usuario() {
    }

    public Usuario(String dni, String nombre, String apellidos,String telefono, String movil, String email, String numerotarjeta, String fecha_cad, String password, short fit_points,ArrayList<Oferta> misofertas) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.setTelefono(telefono);
        this.setMovil(movil);
        this.email = email;
        this.numerotarjeta = numerotarjeta;
        this.setFecha_cad(fecha_cad);
        this.password = password;
        this.fit_points = fit_points;
        this.misofertas=misofertas;
    }

   

    public ArrayList<Oferta> getMisofertas() {
        return misofertas;
    }

    public void setMisofertas(ArrayList<Oferta> misofertas) {
        this.misofertas = misofertas;
    }
    

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
    	if (!telefono.equals("")) {
           this.telefono = Integer.parseInt(telefono);
       }else {
    	   this.telefono=0;
       }
    	   
    }

    public int getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil =Integer.parseInt(movil);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumerotarjeta() {
        return numerotarjeta;
    }

    public void setNumerotarjeta(String numerotarjeta) {
        this.numerotarjeta = numerotarjeta;
    }

    public LocalDate getFecha_cad() {
        return fecha_cad;
    }

    public void setFecha_cad(String fecha) {
    	LocalDate fn=LocalDate.parse(fecha);
        this.fecha_cad = fn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public short getFit_points() {
        return fit_points;
    }

    public void setFit_points(short fit_points) {
        this.fit_points = fit_points;
    }
    
  
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitpoint;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author 1DAM
 */
final public class Gimnasio {
    private String CIF;
    private String nombre;
    private String direccion;
    private String gerente;
    private int telefono;
    private String email;
    private String cc;
    private int id_gimnasio;
    private String password;
   /*  private ArrayList<Oferta> misofertas=new ArrayList<Oferta>();  */
   

    public Gimnasio() {
    }

    public Gimnasio(String CIF, String nombre, String direccion, String gerente, int telefono, String email, String cc, String password,int id) {
        this.CIF = CIF;
        this.nombre = nombre;
        this.direccion = direccion;
        this.gerente = gerente;
        this.telefono = telefono;
        this.email = email;
        this.cc = cc;
        this.password = password;
        this.id_gimnasio=id;  
     
    }

 
    

    public String getCIF() {
        return CIF;
    }

    public void setCIF(String CIF) {
        this.CIF = CIF;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getGerente() {
        return gerente;
    }

    public void setGerente(String gerente) {
        this.gerente = gerente;
    }

   

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

   

  public int getId_gimnasio() {
        return id_gimnasio;
    }

    public void setId_gimnasio(int id_gimnasio) {
        this.id_gimnasio = id_gimnasio;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    
    
    
}

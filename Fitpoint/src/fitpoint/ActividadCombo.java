package fitpoint;
/***Clase que he creado para rellenar un combobos que se vea el nombre y devuelva el indice*/

final public class ActividadCombo {
	 private String nombre;
	  private int id ;
	  
	  public ActividadCombo(String nombre , int id ) {
	    this.nombre=nombre;
	    this.id=id;
	  }
	 
	  public int getID(){
	    return id ;
	  }
	 
	  public String toString() {
	    return nombre ;
	  }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitpoint;

import java.sql.Date;
import java.sql.Time;

/**En esta clase los Gimnasios podrán ofertar las actividades..indicando la hora,fecha y plazas ofertadas
 *
 * @author 1DAM
 */
final public class Oferta {
    private Actividad id_actividad;
    private Gimnasio id_gimnasio;
    private Time hora ;
    private int id_oferta;
    private short plazas;
    private Date fecha;
    private short num_fitpoints;

    public Oferta() {
    }

    public Oferta(Actividad id_actividad, Gimnasio id_gimnasio, Time hora, int id_oferta, short plazas, Date fecha,short num_fitpoints) {
        this.id_actividad = id_actividad;
        this.id_gimnasio = id_gimnasio;
        this.hora = hora;
        this.id_oferta = id_oferta;
        this.plazas = plazas;
        this.fecha = fecha;
        this.num_fitpoints=num_fitpoints;
    }

    public short getNum_fitpoints() {
        return num_fitpoints;
    }

    public void setNum_fitpoints(short num_fitpoints) {
        this.num_fitpoints = num_fitpoints;
    }
    

    public Actividad getId_actividad() {
        return id_actividad;
    }

    public void setId_actividad(Actividad id_actividad) {
        this.id_actividad = id_actividad;
    }

    public Gimnasio getId_gimnasio() {
        return id_gimnasio;
    }

    public void setId_gimnasio(Gimnasio id_gimnasio) {
        this.id_gimnasio = id_gimnasio;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public int getId_oferta() {
        return id_oferta;
    }

    public void setId_oferta(int id_oferta) {
        this.id_oferta = id_oferta;
    }

    public short getPlazas() {
        return plazas;
    }

    public void setPlazas(short plazas) {
        this.plazas = plazas;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
    
}

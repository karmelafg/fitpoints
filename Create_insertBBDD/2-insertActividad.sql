/*
-- Query: SELECT * FROM fitpoints3.actividad
LIMIT 0, 1000

-- Date: 2019-05-31 18:09
*/
INSERT INTO `actividad` (`id_actividad`,`nombre`,`descripcion`) VALUES (1,'Body Pump','El Body Pump es la clase original con barra y discos que fortalece y tonifica todo el cuerpo. En esta sesión, de 60 minutos, trabajarás los principales grupos musculares utilizando los mejores ejercicios de la sala de fitness');
INSERT INTO `actividad` (`id_actividad`,`nombre`,`descripcion`) VALUES (2,'Body Combat','El Body Combat es un programa de entrenamiento cardiovascular inspirado en las Artes Marciales, con el que liberarás grandes dosis de adrenalina y descubrirás tu fuerza interior. Sus movimientos derivan de una gran variedad de disciplinas tales como Karate, Boxeo, Tae Kwon Do, Tai Chi y Muay Thai. C');
INSERT INTO `actividad` (`id_actividad`,`nombre`,`descripcion`) VALUES (4,'Body Balance','El Body Balance es un programa de entrenamiento inspirado en el Yoga, el Tai Chi y el Pilates que mejora la flexibilidad, la fuerza y te aportará una sensación de calma y bienestar.');
INSERT INTO `actividad` (`id_actividad`,`nombre`,`descripcion`) VALUES (5,'Pilates','El pilates es una disciplina centenaria que combina ejercicios de equilibrio y resistencia que mejoran el cuerpo y la mente en pocas semanas. ');
INSERT INTO `actividad` (`id_actividad`,`nombre`,`descripcion`) VALUES (6,'Aerobox','El aerobox es una actividad energética que permite endurecer los músculos, evitar enfermedades cardiovasculares y modelar el cuerpo mediante la realización de ejercicios relacionados con el kickboxing (puños y patadas) acompañados también con música. ');

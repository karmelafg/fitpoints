/*
-- Query: SELECT * FROM fitpoints3.gimnasio
LIMIT 0, 1000

-- Date: 2019-05-31 18:18
*/
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (1,'Valls Sport','C/Meosneros Romanos','123456H','1231231234','vals@hotmail.com',952301336,'Federico Gonzalez','1111');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (2,'Full Sport','C/Andalucía 33','12343423k','111111234','full@hotmail.com',952281135,'Marta Jimenez','2222');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (3,'Sportmanía','C/Estrella 12','14122567K','33333333333','sportmania@hotmail.com',952141285,'Pepa García','3333');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (4,'SmartFit','Av Juan XXIII 23','12677767K','11111111','smartfit@hotmail.com',65555555,'Jose Bueno','4444');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (5,'Curves','c/Carril del capitan 25','77898985L','222222','curves@hotmail.com',655252525,'Paco Martinez','55555');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (6,'Synergym','C/Frances Bosco 25','7788977P','564644','synergim@hotmail.com',952141414,'Saul Castillejo','6666');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (7,'Go It','av/Antonio Machado','4800957P','47474747','goit@hotmail.com',689898989,'Angel Castillejo','7777');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (8,'InGym','C/Juan Juliani 17','1400987P','22222','ingym@hotmail.com',658474747,'Gloria Fernandez','8888');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (9,'Robles','C/Juan Padilla','2135346P','123456','robles@hotmail.com',951232323,'Monica García','9999');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (10,'Vals Sport Consul','C/Sofocles 11','7896542L','789654','valsconsul@hotmail.com',952303030,'Maribel Molina','1010');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (11,'O2 Perchel','C/Plaza de Toros Vieja ,5','53686825Ñ','456789','o2perchel@hotmail.com',952363900,'Fina Fernande','1011');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (12,'Krono','C/Juan Margarit 4','789524M','8765489','krono@hotmail.com',952696952,'Gelen Marchamalo','1012');
INSERT INTO `gimnasio` (`id_gimansio`,`nombre`,`direccion`,`CIF`,`cc`,`email`,`telefono`,`gerente`,`password`) VALUES (13,'Aviva','Av Pegregalejo 14','565656L','8798465','aviva@hotmail.com',951454545,'Ruben Avila','1013');

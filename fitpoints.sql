-- MySQL Script generated by MySQL Workbench
-- Thu Apr  4 10:22:58 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Usuario` (
  `nombre` VARCHAR(20) NOT NULL,
  `apellido1` VARCHAR(45) NOT NULL,
  `apellido2` VARCHAR(45) NOT NULL,
  `telefono` INT(12) NULL,
  `movil` INT(9) NOT NULL,
  `email` VARCHAR(20) NOT NULL,
  `password` VARCHAR(10) NOT NULL,
  `num_tarjeta` VARCHAR(16) NOT NULL,
  `fecha_caducidad` DATE NOT NULL,
  `dni` VARCHAR(10) NULL,
  `fit_points` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`dni`),
  UNIQUE INDEX `usuario_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Gimnasio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Gimnasio` (
  `id_gimansio` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  `CIF` VARCHAR(45) NOT NULL,
  `fit_points_acu` INT NOT NULL,
  `cc` VARCHAR(15) NOT NULL,
  `email` VARCHAR(45) NULL,
  `telefono` VARCHAR(45) NOT NULL,
  `gerente` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_gimansio`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`table1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`table1` (
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Actividad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Actividad` (
  `id_actividad` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(300) NOT NULL,
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE,
  PRIMARY KEY (`id_actividad`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Oferta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Oferta` (
  `Actividades_id_actividad` INT NOT NULL,
  `Gimnasio_id_gimansio` INT NOT NULL,
  `hora` VARCHAR(45) NOT NULL,
  `fecha` DATE NOT NULL,
  `id_oferta` INT NOT NULL AUTO_INCREMENT,
  `plazas` INT NULL DEFAULT 0,
  INDEX `fk_Actividades_has_Gimnasio_Gimnasio1_idx` (`Gimnasio_id_gimansio` ASC) VISIBLE,
  INDEX `fk_Actividades_has_Gimnasio_Actividades_idx` (`Actividades_id_actividad` ASC) VISIBLE,
  PRIMARY KEY (`id_oferta`),
  CONSTRAINT `fk_Actividades_has_Gimnasio_Actividades`
    FOREIGN KEY (`Actividades_id_actividad`)
    REFERENCES `mydb`.`Actividad` (`id_actividad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Actividades_has_Gimnasio_Gimnasio1`
    FOREIGN KEY (`Gimnasio_id_gimansio`)
    REFERENCES `mydb`.`Gimnasio` (`id_gimansio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Compras` (
  `Usuario_dni` VARCHAR(10) NOT NULL,
  `Ofertas_id_oferta` INT NOT NULL,
  PRIMARY KEY (`Usuario_dni`, `Ofertas_id_oferta`),
  INDEX `fk_Usuario_has_Ofertas_Ofertas1_idx` (`Ofertas_id_oferta` ASC) VISIBLE,
  INDEX `fk_Usuario_has_Ofertas_Usuario1_idx` (`Usuario_dni` ASC) VISIBLE,
  CONSTRAINT `fk_Usuario_has_Ofertas_Usuario1`
    FOREIGN KEY (`Usuario_dni`)
    REFERENCES `mydb`.`Usuario` (`dni`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_has_Ofertas_Ofertas1`
    FOREIGN KEY (`Ofertas_id_oferta`)
    REFERENCES `mydb`.`Oferta` (`id_oferta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
